# Intelibus
## Sobre
O Intelibus é um sistema web e mobile responsável pelo controle de linhas de ônibus em tempo real, além de possibilitar o pagamento pelo aplicativo.
Oferece também possibilidade de recarga e a reutilização de crédito em diferentes linhas de diferentes cidades, eliminando a necessidade
de se obter um cartão de passe para diferentes empresas e cidades.

## A aplicação
A aplicação consiste numa plataforma web utilizando ReactJS que se comunica com uma Web API em Asp.NET Core, utilizando conceitos de REST
e web sockets para comunicação entre cliente e servidor e no monitoramento georreferencial de veículos de transporte coletivo.

O sistema ainda possui algoritmos para tomada de decisão e manipulação e análise de dados utilizando a linguagem R. Além de guardar
todos os dados em um banco de dados não apenas relacional.

## Aviso!
O projeto exposto no BitBucket apenas apresenta a interface web feita em ReactJS, não sendo possível a visualização do código utilizado no servidor.

## Online!
Veja um MVP de teste da aplicação rodando em: https://intelibus.azurewebsites.net/ 

# 1ª Entrega - Criado projeto web e telas de cadastro
## Features
- Criado controle de acesso. 
- Criado CRUD: usuários, motoristas, veículos.
- Linhas de ônibus no mapa.

## Problemas encontrados
- Cabeçalho não é atualizado após logout do usuário.

## Sugestões
- Diminuir formulário de login
- Mostrar imagem de motoristas

# 2ª Entrega - Criado projeto web e telas de cadastro
## Features
- Mapa em tempo real. 
- Cartão virtual.
- Mostrar caminho no mapa.

## Problemas resolvidos
- Cabeçalho não é atualizado após logout do usuário.

## Problemas encontrados
- Alterar rota não é possível

## Sugestões
- Criar botão de exclusão de rota
- Criar visualização do caminho da linha

# 3ª Entrega - Criado projeto web e telas de cadastro
## Features
- QR Code para pagamento de tarifa.
- Tabela de rotas com melhor controle da empresa
- HotSite na página inicial

## Problemas encontrados
- Modal de definição de rota do usuário quebra o layout.

## Sugestões
- Utilizar ícones de ônibus em vez de marcadores padrão;

# 4ª Entrega - Criado projeto web e telas de cadastro
## Features
- Leitura de QR Code para pagamento. 
- Recuperar Credenciais.

## Problemas encontrados
- Logo do Google Maps está criando margem no painel de caminho feito por uma linha.

## Problemas da versão anterior
- Modal de definição de rota do usuário quebra o layout.
- Alterar rota não é possível.

## Sugestões
- Diminuir e centralizar formulários 
- Listar empresas para os usuários


@Desenvolvido por Rodrigo G. Rodrigues - Web Developer | MCP #70-483