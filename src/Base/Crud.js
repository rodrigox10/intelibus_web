import Configuration from "../Configuration";
import { sendFetch } from "../Util/Util";
export default class Crud {
    static _baseUri = Configuration.api_url + "/api/";

    // static _baseUri = "http://10.254.207.138:60000/api/";
    constructor(name) {
        this.name = name;
        this.post = Crud.post.bind(this);
        this.get = Crud.get.bind(this);
        this.put = Crud.put.bind(this);
        this.deletar = Crud.deletar.bind(this);
    }

    static get(id, params) {
        const url = Crud._baseUri + this.name;
        let query = "";
        if (params) {
            query = "?";
            for (const key in params) {
                if (params.hasOwnProperty(key)) {
                    const element = params[key];
                    query += `${key}=${element}&`;
                }
            }
        }
        id = id ? "/" + id : "";
        return sendFetch({ url: url + id + query, method: "GET" });
    }

    static post(obj) {
        let model = obj, url = "";
        let headers = null;
        if (obj && obj.url) {
            headers = obj.headers;
            if (obj.body) {
                model = obj.body;
            } else {
                model = null;
            }
            url = obj.url;
        }
        const body = model || this;
        const uri = Crud._baseUri + this.name;
        // console.log("enviando com body: ", body);
        const request = url ? uri + "/" + url : uri;
        return sendFetch({ url: request, method: "POST", body, headers });
    }

    static put(obj) {
        let model = obj, url = "";
        if (obj && obj.url && obj.model) {
            model = obj.model;
            url = obj.url;
        }
        const body = model || this;
        const uri = Crud._baseUri + this.name;
        const request = url ? uri + "/" + url : uri;
        return sendFetch({ url: request, method: "PUT", body });
    }

    static deletar(id) {
        const uri = Crud._baseUri + this.name;
        id = id ? "/" + id : "";
        return sendFetch({ url: uri + id, method: "DELETE" });
    }

}