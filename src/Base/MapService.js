import lodash from "lodash";
import { simpleExtend } from "../Util/Util";

export default class MapaService {
    constructor(mapa) {
        const google = window.google;
        var self = this;
        if (!mapa || typeof mapa !== "object")
            throw new Error("Por favor, carregue o mapa antes de instanciar um MapaService.\nNão é possível criar uma instancia de MapaService com: ", mapa);
        self.selectionMarker = new google.maps.Marker();
        self.directions = new google.maps.DirectionsService();
        self.geocoder = new google.maps.Geocoder();
        self.map = mapa;
        // self.genericTooltip = genericTooltip;
        self.options = mapa.options;
        self.markers = mapa.markers = mapa.markers || [];
        self.polylines = mapa.polylines = mapa.polylines || [];
        self.polygons = mapa.polygons = mapa.polygons || [];
        self.circles = mapa.circles = mapa.circles || [];
        self.kmls = mapa.kmls = mapa.kmls || [];
        // self.POSITION = POSITION;
        /* INTERNAL FUNCTIONS - funções privadas do serviço */
        function getAnchor(m) {
            var mx = 0, my = 0, vertical, horizontal, x, y;
            if (!m.fontAwesome) {
                x = m.width;
                y = m.height;
                mx = Math.floor(x / 2);
                my = Math.floor(y / 2);

                vertical = parseInt(m.iconPosition / 3);
                horizontal = m.iconPosition % 3;
                switch (vertical) {
                    case 1: // Centro
                        y -= my;
                        break;
                    case 2: // inferior
                        y = 0;
                        break;
                }
                switch (horizontal) {
                    // case 2: Direito
                    case 1: // Centro
                        x -= mx;
                        break;
                    case 2: // Esquerdo
                        x = 0;
                        break;
                }
                return { x: x, y: y };
            }
            else {
                mx = Math.floor(m.marker.clientWidth / 2);
                my = Math.floor(m.fontSize / 2);
                var point = m.getProjection().fromLatLngToDivPixel(m.latlng);
                if (point) {
                    x = point.x;
                    y = point.y;
                    vertical = parseInt(m.iconPosition / 3);
                    horizontal = m.iconPosition % 3;
                    switch (vertical) {
                        case 1: // Centro
                            y -= my;
                            break;
                        case 0: // Superior
                            y -= m.fontSize;
                            break;
                    }
                    switch (horizontal) {
                        // case 2: Direito
                        case 1: // Centro
                            x -= mx;
                            break;
                        case 0: // Direito
                            x -= m.marker.clientWidth;
                            break;
                    }
                    return { x: x, y: y };
                }
            }
        }
        /**
         * Processa todos os eventos de cada elemento, os eventos devem estar definidos no atributo events do elemento
         * @param {any} i elemento do mapa que contém o atributo events que receberá os listeners.
         */
        function processEvents(i) {
            /*if (i.iefTooltip && !i.fontAwesome) {
                google.maps.event.addListener(i, 'mouseover', function (e) {
                    self.genericTooltip.show(e, i.iefTooltip);
                });

                google.maps.event.addListener(i, 'mousemove', function (e) {
                    self.genericTooltip.move(e);
                });
                google.maps.event.addListener(i, 'mouseout', function () {
                    self.genericTooltip.hide();
                });
            }
            lodash.forOwn(i.events, function (f, e) {
                i.addListener(e, f);
            });*/
        }
        /**
         * Retorna a posição do click do mapa de acordo com a posição atual passada por parâmetro
         * @param {any} currentLatLng Posição do click na tela
         * @returns {google.maps.LatLng} retorna um objeto contendo a posição geográfica do clique no mapa
         */
        function getCanvasXY(currentLatLng) {
            var map = self.map;
            var scale = Math.pow(2, map.getZoom());
            var nw = new google.maps.LatLng(map.getBounds().getNorthEast().lat(), map.getBounds().getSouthWest().lng());
            var worldCoordinateNW = map.getProjection().fromLatLngToPoint(nw);
            var worldCoordinate = map.getProjection().fromLatLngToPoint(currentLatLng);
            var currentLatLngOffset = new google.maps.Point(
                Math.floor((worldCoordinate.x - worldCoordinateNW.x) * scale),
                Math.floor((worldCoordinate.y - worldCoordinateNW.y) * scale)
            );
            return currentLatLngOffset;
        }
        /**
         * Posiciona o marker de seleção na última posição do caminho passado por parâmetro
         * @param {any} path array com os pontos que se deseja obter a última posição ou a própria posição que se deseja colocar o marker
         * @returns {google.maps.LatLng} retorna uma instância de LatLng contendo a última posição do caminho, além de alterar a posição do selectionMarker para a posição retornada
         */
        function setLastPositionPath(path) {
            var pos = path[path.length - 1];
            if (path instanceof google.maps.LatLng) {
                pos = path;
            }
            else {
                pos = new google.maps.LatLng({ lat: pos.lat || pos.latitude, lng: pos.lng || pos.longitude });
            }
            self.selectionMarker.setPosition(pos);
            self.selectionMarker.setMap(self.map);
            return pos;
        }
        function setContextMenuXY(currentLatLng) {
            var mapElement = self.map.getDiv();
            var mapWidth = mapElement.clientWidth;
            var mapHeight = mapElement.clientHeight;
            var style = self._contextMenu.style;
            var menuWidth = self._contextMenu.clientWidth;
            var menuHeight = self._contextMenu.clientHeight;
            var clickedPosition = getCanvasXY(currentLatLng);
            var x = clickedPosition.x;
            var y = clickedPosition.y;

            if (mapWidth - x < menuWidth) {
                x = x - menuWidth;
            }
            if (mapHeight - y < menuHeight) {
                y = y - menuHeight;
            }

            style['left'] = x + "px";
            style['top'] = y + "px";
        }

        function findItem(list, item) {
            if (item) {
                return lodash.find(self[list], { id: item });
            }
            return item;
        }

        function removeItem(list, item) {
            if (item) {
                if (typeof item !== "object") {
                    item = findItem(list, item);
                }
                item.setMap(null);
                var i = self[list].indexOf(item);
                if (i != -1) {
                    self[list].splice(i, 1);
                }
                if (item._infoBox) {
                    item._infoBox.close();
                }
            }
            return item;
        }

        self.getAllElements = function () {
            var x = [];
            x = x.concat(self.markers, self.polylines, self.circles, self.polygons);
            return x;
        };

        self.retornaItem = function (item) {
            var id = item.id || item;
            return findItem("markers", id)
                || findItem("polygons", id)
                || findItem("polylines", id)
                || findItem("circles", id);
        };

        self.addEventListener = function (name, func) {
            google.maps.event.addListener(self.map, name, func);
        };

        self.getPolygonCenter = function (poly) {
            var id = poly.id || poly, center_x, center_y;
            poly = findItem("polygons", poly);
            var lowx, highx, lowy, highy,
                lats = [], lngs = [], vertices = poly.getPath();

            for (var i = 0; i < vertices.length; i++) {
                lngs.push(vertices.getAt(i).lng());
                lats.push(vertices.getAt(i).lat());
            }

            lats.sort(function (lA, lB) { return lA - lB; });
            lngs.sort(function (lA, lB) { return lA - lB; });

            lowx = lodash.first(lats);
            highx = lodash.last(lats);
            lowy = lodash.first(lngs);
            highy = lodash.last(lngs);
            center_x = lowx + (highx - lowx) / 2;
            center_y = lowy + (highy - lowy) / 2;

            return new google.maps.LatLng(center_x, center_y);
        };

        self.setInfoBox = function (item, body) {
            var anchor;
            switch (item.itemType) {
                case "marker":
                    anchor = item.getPosition();
                    break;
                case "polygon":
                    anchor = self.getPolygonCenter(item);
                    break;
                case "polyline":
                    var path = item.getPath();
                    anchor = path.getAt(Math.floor(path.length / 2));
                    break;
                case "circle":
                    anchor = item.getCenter();
                    break;
            }
            var labelOptions = {
                content: body,
                //map: self.map,
                boxStyle: {
                    textAlign: "center",
                    fontSize: "8pt",
                    width: "50px"
                },
                disableAutoPan: true,
                pixelOffset: new google.maps.Size(0, 0),
                maxWidth: 200,
                position: anchor,
                closeBoxURL: "",
                pane: "mapPane",
                enableEventPropagation: true
            };
            var infoBox = new google.maps.InfoWindow(labelOptions);
            item._infoBox = infoBox;
            if (item.hasOwnProperty("visible") && item.visible === false) {
                infoBox.close();
            }
            else {
                infoBox.open(self.map);
            }
            item.addListener("click", function () {
                infoBox.open(self.map);
            });
        };

        self.setContextMenu = function (id) {
            self._contextMenu = document.getElementById(id);
            self.contextMenu = {
                open: function (e) {
                    if (!self._contextMenu) {
                        self._contextMenu = document.getElementById(id);
                        // às vezes, o menu de contexto abre antes do contextmenu disparar no mapa, ou seja, o evento acaba sendo disparado no próprio elemento do context menu
                        self._contextMenu.addEventListener("contextmenu", function (e) { // impede que abra o context-menu do browser
                            e.preventDefault();
                        });
                        return;
                    }

                    self._contextMenu.style.display = "block";

                    setContextMenuXY(e.latLng);
                },
                close: function (e) {
                    if (self._contextMenu) self._contextMenu.style.display = "none";
                }
            };

            self.addEventListener("rightclick", function (ev) {
                setTimeout(function () {
                    self.contextMenu.open(ev);
                }, 100);
            });

            self.map.getDiv().addEventListener("click", self.contextMenu.close);
            //console.log(self.map.getDiv());
            self.map.getDiv().addEventListener("contextmenu", function (e) { // impede que abra o context-menu do browser
                e.preventDefault();
            });
        };

    /* POLIGONOS */ {
            self.updatePolygon = function (dados) {
                var item = lodash.find(self.polygons, { id: dados.id });
                if (item) {
                    item.setOptions(dados);
                }
            };

            var returnPolygon = function (drawing, callback, path, editting) {
                var res = window.confirm("menu.mensagem.usar.o.poligono.determinado");
                drawing.setMap(null);
                if (res) {
                    return callback(self.getPolylineCoordinates(drawing));
                } else if (editting) {
                    drawing.setMap(self.map);
                    return self.editPolygon(drawing, callback);
                } else {
                    return self.addPolygon(callback, path);
                }
            };

            self.editPolygon = function (polygon, callback) {
                var id = polygon.id || polygon;
                var item = lodash.find(self.polygons, { id: id });
                if (!item) {
                    return callback(null);
                }
                item.setOptions({ editable: true });
                var path = item.getPath();
                var init = path.getAt(0);
                self.selectionMarker.setOptions({ map: self.map, position: init });
                google.maps.event.addListenerOnce(self.selectionMarker, "click", function (e) {
                    // when click marker, close the polygon
                    self.selectionMarker.setMap(null);
                    return returnPolygon(item, callback, null, true);
                });
            };
            self.addPolygon = function (callback, path, options) {
                self.warningAlert = alert("menu.mensagem.clique.marcador.encerrar.desenho");
                var initialPosition = [];
                if (path && path.length) {
                    //https://jsfiddle.net/b8vd96ce/12/
                    var pos = setLastPositionPath(path);
                    initialPosition.push(pos);
                } else {
                    google.maps.event.addListenerOnce(self.map, 'click', function (event) {
                        setLastPositionPath(event.latLng);
                    });
                }

                var div = self.getMapDiv();
                var polyline = new google.maps.Polyline({ // init drawing as polyline
                    path: initialPosition,
                    map: self.map
                });
                var polygon = null;
                var limit = options && options.limitIntersections;
                var finalizarDesenho = function (e) {
                    // when click marker, close the polygon
                    self.selectionMarker.setMap(null);
                    polyline.setMap(null);
                    polygon = new google.maps.Polygon({ path: polyline.getPath(), map: self.map });
                    polygon.intersections = polyline.intersections;
                    listener.remove(); // stop drawing polyline
                    div.className = div.className.replace(/ ?drawing/, "");
                    //self.warningAlert && self.warningAlert.close();
                    return returnPolygon(polygon, callback, path);
                };

                var clickHandler = function (event) {
                    var path = polyline.getPath();
                    path.push(event.latLng);

                    if (limit && limit == polyline.intersections.length) {
                        finalizarDesenho(event);
                    }
                };

                var listener = google.maps.event.addListener(self.map, 'click', clickHandler);

                //addConnectionListeners(polyline, clickHandler);

                div.className += " drawing";
                return google.maps.event.addListenerOnce(self.selectionMarker, "click", finalizarDesenho);
            };

            self.addPolygonOnMap = function (polygon) {
                polygon.paths.forEach(function (path) {
                    path.lat = path.lat || path.latitude;
                    path.lng = path.lng || path.longitude;
                });
                var p = new google.maps.Polygon(polygon);
                processEvents(p);
                p.itemType = "polygon";
                p.setMap(self.map);
                self.map.polygons.push(p);
                return p;
            };

            self.removePolygon = function (polygon) {
                return removeItem("polygons", polygon);
            };
        }
    /* MARKERS */{
            self.updateMarker = function (dados) {
                var item = lodash.find(self.markers, { id: dados.id });
                if (item) {
                    if (item.fontAwesome) {
                        item.update(dados);
                    }
                    else {
                        item.setOptions(dados);
                    }
                }
            };

            self.editMarker = function (id, callback) {
                var marker = findItem("markers", id);
                if (marker) {
                    marker.setOptions({
                        draggable: true
                    });
                    google.maps.event.addListenerOnce(marker, "dragend", function () {
                        returnMarker(marker.getPosition(), callback);
                    });
                }
            };
            var returnMarker = function (position, callback) {
                window.confirm("menu.mensagem.usar.o.marcador.determinado", function (res) {
                    self.selectionMarker.setMap(null);
                    if (res) {
                        self.selectionMarker.latitude = position.lat();
                        self.selectionMarker.longitude = position.lng();
                        return callback(self.selectionMarker);
                    } else {
                        return self.addMarker(callback);
                    }
                });
            };

            self.addMarker = function (callback, path) {
                var div = self.getMapDiv();
                div.className += " drawing";
                if (path && path.length) {
                    var pos = setLastPositionPath(path);
                    return setTimeout(function () {
                        div.className = div.className.replace(/ ?drawing/, "");
                        self.selectionMarker.latitude = self.selectionMarker.position.lat();
                        self.selectionMarker.longitude = self.selectionMarker.position.lng();
                        callback(self.selectionMarker);
                        self.selectionMarker.setMap(null);
                    }, 1000);
                }
                var listener = google.maps.event.addListenerOnce(self.map, 'click', function (event) {
                    div.className = div.className.replace(/ ?drawing/, "");
                    setLastPositionPath(event.latLng);
                    setTimeout(function () {
                        return returnMarker(event.latLng, callback);
                    }, 500);
                });
            };

            self.addMarkerOnMap = function (marker) {
                if (marker.latitude || marker.longitude) {
                    var position = {
                        lat: marker.latitude,
                        lng: marker.longitude
                    };
                    marker.position = position;
                }
                var m;
                if (marker.icon) {
                    if (marker.icon.match(/^https?:/)) {
                        var anchor = getAnchor(marker);
                        var image = {
                            url: marker.icon,
                            fontSize: 18,
                            scaledSize: new google.maps.Size(marker.width, marker.height),
                            anchor: new google.maps.Point(anchor.x, anchor.y)
                        };
                        marker.icon = image;
                        m = new google.maps.Marker(marker);
                    }
                } else {
                    m = new google.maps.Marker(marker);
                }
                processEvents(m);
                m.itemType = "marker";
                m.setMap(self.map);
                self.map.markers.push(m);
                return m;
            };
            self.removeMarker = function (marker) {
                return removeItem("markers", marker);
            };
            self.getMarker = function (id) {
                return findItem("markers", id);
            };
        }
    /* CIRCULOS */{

            var returnCircle = function (circle, callback, path, editting) {
                window.confirm("menu.mensagem.usar.o.circulo.determinado", function (res) {
                    circle.setMap(null);
                    if (res) {
                        circle.latitude = circle.center.lat();
                        circle.longitude = circle.center.lng();
                        return callback(circle);
                    } else if (editting) {
                        circle.setMap(self.map);
                        self.editCircle(circle, callback);
                    } else {
                        return self.addCircle(callback, path);
                    }
                });
            };
            self.editCircle = function (circle, callback) {
                var c = lodash.find(self.circles, { id: circle.id });
                if (c) {
                    c.setOptions({ editable: true });
                    setLastPositionPath([c]);

                    return google.maps.event.addListenerOnce(self.selectionMarker, "click", function (e) {
                        // when click marker, close the circle
                        c.setOptions({ editable: false }); // stop editting circle
                        self.selectionMarker.setMap(null);
                        return returnCircle(c, callback, null, true);
                    });
                }
                return callback();
            };
            /**
             * Desenha um circulo no mapa e retorna o desenho via callback
             * @param {Function} callback função que receberá como único parâmetro a instância de um Circle, resultado do desenho feito
             * @param {any} path recebe o ponto inicial ou array de pontos ao qual o desenho será posto inicialmente, caso o usuário deva escolher o ponto, não passar esse parâmetro
             * @returns {any} retorna o listener do marker que será clicado para finalizar o desenho
             */
            self.addCircle = function (callback, path) {
                if (!self.options.drawingManagerControl) {
                    throw Error("Nenhum Drawing Manager especificado para gerar o polígono. Por favor, referencie a Drawing Manager Control no mapa para a propriedade 'drawingManagerControl' da instancia do mapa.\nProperty 'drawingManagerControl' não pode ser undefined!");
                }
                self.warningAlert = alert("menu.mensagem.clique.marcador.encerrar.desenho");
                var initial = null;
                if (path && path.length) {
                    // https://jsfiddle.net/b8vd96ce/14/
                    var pos = setLastPositionPath(path);
                    initial = pos;
                } else {
                    google.maps.event.addListenerOnce(self.map, "click", function (e) {
                        // when click marker, close the circle
                        circulo.setOptions({ editable: true, center: e.latLng });
                        setLastPositionPath(e.latLng);
                    });
                }

                var div = self.getMapDiv();
                var circulo = new google.maps.Circle({
                    center: initial,
                    editable: true,
                    radius: 1200,
                    map: self.map
                });

                div.className += " drawing";

                return google.maps.event.addListenerOnce(self.selectionMarker, "click", function (e) {
                    // when click marker, close the circle
                    circulo.setOptions({ editable: false }); // stop editting circle
                    self.selectionMarker.setMap(null);
                    div.className = div.className.replace(/ ?drawing/, "");
                    self.warningAlert && self.warningAlert.close();
                    return returnCircle(circulo, callback, path);
                });
            };

            self.updateCircle = function (dados) {
                var item = lodash.find(self.circles, { id: dados.id });
                if (item) {
                    item.setOptions(dados);
                }
            };

            self.addCircleOnMap = function (circle) {
                var options = simpleExtend({
                    center: { lat: circle.latitude, lng: circle.longitude },
                    radius: circle.raio
                }, circle);

                var c = new google.maps.Circle(options);
                processEvents(c);
                c.itemType = "circle";
                c.setMap(self.map);
                self.map.circles.push(c);
                return c;
            };
            self.removeCircle = function (circle) {
                return removeItem("circles", circle);
            };
        }
    /* POLYLINES */{
            var returnPolyline = function (drawing, callback, path, editting) {
                var res = window.confirm("menu.mensagem.usar.a.polilinha.determinada");
                drawing.setMap(null);
                // cleanListeners();
                if (res) {
                    return callback(self.getPolylineCoordinates(drawing));
                } else if (editting) {
                    drawing.setMap(self.map);
                    return self.editPolyline(drawing, callback);
                } else {
                    if (drawing.streetPolyline) {
                        return self.addStreetPolyline(callback, path);
                    }
                    return self.addPolyline(callback, path);
                }
            };

            self.updatePolyline = function (dados) {
                var item = lodash.find(self.polylines, { id: dados.id });
                if (item) {
                    item.setOptions(dados);
                }
                return item;
            };

            self.editPolyline = function (polyline, callback) {
                var id = polyline.id || polyline;
                var item = lodash.find(self.polylines, { id: id });
                if (!item) {
                    return callback(null);
                }
                item.setOptions({ editable: true });
                var path = item.getPath();
                var init = path.getAt(0);
                self.selectionMarker.setOptions({ map: self.map, position: init });
                google.maps.event.addListenerOnce(self.selectionMarker, "click", function (e) {
                    // when click marker, close the polygon
                    self.selectionMarker.setMap(null);
                    return returnPolyline(item, callback, null, true);
                });
            };
            var getRoute = function (from, to, waypoints, callback) {
                var waypointsString = "";
                if (waypoints && waypoints.length) {
                    waypointsString = waypoints.join("|");
                }
                var origin = [from.lat(), from.lng()].join(",");
                var destination = [to.lat(), to.lng()].join(",");
                var travelMode = "WALKING";
                var params = { origin: origin, destination: destination, travelMode: travelMode };
                if (waypointsString) {
                    params["waypoints"] = waypointsString;
                }
                return self.directions.route(params, callback);
            };
            var tracarRota = function (p, callback) {
                if (p.caminho.length >= 2) {
                    var pontosRota = p.caminho;
                    var rota = pontosRota.slice(pontosRota.length - 2);
                    var concatLocation = function (latlng) {
                        return latlng.lat().toString() + ";" + latlng.lng().toString();
                    };
                    //var origin = concatLocation(rota[0]);
                    //var destination = concatLocation(rota[1]);
                    var origin = rota[0].geometry.location;
                    var destination = rota[1].geometry.location;
                    return getRoute(origin, destination, null, function (retorno, status) {
                        var route = retorno.routes[0];
                        var path = google.maps.geometry.encoding.decodePath(route.overview_polyline);
                        var ps = new google.maps.Polyline({ path: path });
                        var caminho = p.pontos;
                        p.paths = p.paths || [];
                        p.lines = p.lines || [];

                        p.lines.push(ps);
                        p.paths.push(path);
                        ps.setMap(self.map);
                        // $rootScope.$emit("pararLoading");
                        callback && callback();
                    });
                }
                // $rootScope.$emit("pararLoading");
                return callback && callback();
            };
            /**
             * Desenha uma polilinha no mapa contornando as ruas e retorna o desenho via callback
             * @param {Function} callback função que receberá como único parâmetro a instância de um Polyline, resultado do desenho feito
             * @param {any} path recebe o ponto inicial ou array de pontos ao qual o desenho será posto inicialmente, caso o usuário deva escolher o ponto, não passar esse parâmetro
             * @param {{limitIntersections:Number}} options opções sobre a criação da polilinha (limitIntersections - inteiro contendo a quantidade máxima de intersecções que o
             * elemento deve ter, ou seja, quando atingir o limite o desenho encerrará sozinho, deixe 0 ou não informe esse valor para permitir intersecções ilimitadas)
             * @returns {any} retorna o listener do marker que será clicado para finalizar o desenho
             */
            self.addStreetPolyline = function (callback, path, options) {
                var polyline = { caminho: [], paths: [], lines: [] };
                var limit = options && options.limitIntersections;

                var onClickMap = function (event) {
                    setLastPositionPath(event.latLng);
                    // $rootScope.$emit("iniciarLoading");
                    /*polyline.caminho.push(event.latLng);
                    tracarRota(polyline);*/
                    self.getStreet(event.latLng, function (res) {
                        if (polyline.ended) {
                            return;
                        }
                        // Por enquanto, o parametro result_type não está disponível para o geocode API, então é necessário filtrar aqui mesmo
                        var streets = lodash.filter(res, function (r) {
                            r.distancia = self.getDistanceBetween(event.latLng, r.geometry.location); // calcula a distancia da rua encontrada para o ponto no mapa
                            return r.types.indexOf("street_address") > -1;
                        });

                        streets = lodash.sortBy(streets, "distancia"); // ordena de forma ascendente pela distância calculada
                        //console.log(streets);
                        var first = lodash.first(streets); // pega o primeiro resultado q, no caso, é o ponto mais próximo
                        polyline.caminho.push(first);
                        if (limit && limit == polyline.intersections.length) {
                            polyline.ended = true; // avoid multiple events trigger
                            return tracarRota(polyline, function () {
                                // cleanListeners();
                                finalizarDesenho(event);
                            });
                        }
                        return tracarRota(polyline);
                    });
                };

                if (path && path.length) {
                    var pos = setLastPositionPath(path);
                    onClickMap({ latLng: pos });
                }
                var finalizarDesenho = function (e) {
                    self.selectionMarker.setMap(null);
                    listener.remove(); // stop drawing polyline
                    div.className = div.className.replace(/ ?drawing/, "");
                    polyline.path = Array.prototype.concat.apply([], polyline.paths); // concat all paths arrays
                    polyline.lines.forEach(function (ps) { ps.setMap(null); });
                    polyline.streetPolyline = true;
                    var finalPolyline = new google.maps.Polyline(polyline);
                    finalPolyline.intersections = polyline.intersections;

                    self.warningAlert && self.warningAlert.close();
                    return returnPolyline(finalPolyline, callback, path);
                };
                var listener = google.maps.event.addListener(self.map, 'click', onClickMap);
                //addConnectionListeners(polyline, onClickMap);
                var div = self.getMapDiv();
                div.className += " drawing";

                return google.maps.event.addListenerOnce(self.selectionMarker, "click", finalizarDesenho);
            };
            /*self.addStreetPolylineOnMap = function (polyline, callback, options) {
                var path = polyline.getPath ? polyline.getPath().getArray() : polyline.path;
                polyline.caminho = [];

                Async.mapSeries(path, function (p, next) {
                    var latLng = self.latLng(p);
                    self.getStreet(latLng, function (res) {
                        var first = res[0];
                        polyline.caminho.push(first);
                        tracarRota(polyline, next);
                    });
                }, function () {
                    polyline.path = Array.prototype.concat.apply([], polyline.paths);
                    polyline.lines.forEach(function (ps) { ps.setMap(null); });
                    return callback(self.getPolylineCoordinates(polyline));
                });
            };*/

            self.addPolyline = function (callback, path, options) {
                var initialPosition = [];
                self.warningAlert = alert("menu.mensagem.clique.marcador.encerrar.desenho");
                if (path && path.length) {
                    //https://jsfiddle.net/b8vd96ce/13/
                    var pos = setLastPositionPath(path);
                    initialPosition.push(pos);
                }

                var div = self.getMapDiv();
                var polyline = new google.maps.Polyline({ // init drawing as polyline
                    path: initialPosition,
                    map: self.map
                });

                var limit = options && options.limitIntersections;
                var finalizarDesenho = function (e) {
                    // when click marker, close the polygon
                    self.selectionMarker.setMap(null);
                    listener.remove(); // stop drawing polyline
                    self.warningAlert && self.warningAlert.close();
                    div.className = div.className.replace(/ ?drawing/, "");
                    return returnPolyline(polyline, callback, path);
                };
                var clickHandler = function (event) {
                    var path = polyline.getPath();
                    path.push(event.latLng);
                    setLastPositionPath(event.latLng);
                    if (limit && limit == polyline.intersections.length) {
                        finalizarDesenho(event);
                    }
                };
                div.className += " drawing";
                var listener = google.maps.event.addListener(self.map, 'click', clickHandler);

                //addConnectionListeners(polyline, clickHandler);

                return google.maps.event.addListenerOnce(self.selectionMarker, "click", finalizarDesenho);
            };

            self.addPolylineOnMap = function (polyline) {
                polyline.path.forEach(function (path) {
                    path.lat = path.lat || path.latitude;
                    path.lng = path.lng || path.longitude;
                });
                var p = new google.maps.Polyline(polyline);
                processEvents(p);
                p.itemType = "polyline";
                p.setMap(self.map);
                self.map.polylines.push(p);
                return p;
            };
            self.removePolyline = function (polyline) {
                return removeItem("polylines", polyline);
            };
        }

        self.update = function (dados) {
            var item = self.retornaItem(dados);
            if (dados.hasOwnProperty("visible")) {
                if (item._infoBox) {
                    if (dados.visible) {
                        item._infoBox.open(self.map);
                    }
                    else {
                        item._infoBox.close();
                    }
                }
            }
            switch (item.itemType) {
                case "marker":
                    self.updateMarker(dados);
                    break;
                case "polygon":
                    self.updatePolygon(dados);
                    break;
                case "polyline":
                    self.updatePolyline(dados);
                    break;
                case "circle":
                    self.updateCircle(dados);
                    break;
            }
        };

        self.newBounds = function () {
            return new google.maps.LatLngBounds();
        };
        /**
         * Itera todos os pontos preenchendo os campos latitude, longitude e indice
         * @param {any} polyline Polilinha ou poligono desenhado cujos pontos serão iterados
         * @returns {any} retorna a própria polilinha com os pontos gerados
         */
        // um polígono também é uma polilinha, mas uma polilinha não é um polígono
        self.getPolylineCoordinates = function (polyline) {
            if (polyline) {
                polyline.path = polyline.getPath ? polyline.getPath() : polyline.path;
                polyline.path.forEach(function (elem, index) {
                    elem.latitude = elem.lat();
                    elem.longitude = elem.lng();
                    elem.indice = index;
                    return elem;
                });
                return polyline;
            }
            return null;
        };

        self.latLng = function (o) {
            if (o instanceof google.maps.LatLng)
                return o;
            if (o.lat && o.lng) {
                return new google.maps.LatLng(o);
            }
            return new google.maps.LatLng({ lat: o.latitude, lng: o.longitude });
        };
        /**
         * Retorna as localizações das informações passadas por parâmetro
         * @param {any} location string com nome da rua ou uma instância de LatLng contendo a posição que se deseja consultar
         * @param {any} callback função que receberá o resultado da chamada
         * @returns {any} retorna a promise da consulta geocode
         */
        self.getStreet = function (location, callback) {
            var geolocation = "", address;
            if (typeof (location) === 'string' || location instanceof String) {
                if (location.match(/[a-z ]+/g)) {
                    address = location;
                } else {
                    var coords = location.split(",");
                    geolocation = { lat: coords[0], lng: coords[1] };
                }
            }
            else if (location instanceof google.maps.LatLng || location.lat && location.lng) {
                geolocation = location;
            } else {
                return false;
            }
            var params = geolocation ? { "location": geolocation } : { "address": address };
            return self.geocoder.geocode(params, callback);
        };
        self.getMapDiv = function () {
            var d = self.map.getDiv().querySelector("div > div");
            if (!d) {
                throw Error("O mapa não foi encontrado no documento, por favor, certifique-se de que a instância do mapa existe, assim como o elemento no DOM");
            }
            return d;
        };
        /**
         * Calcula da distância da polilinha passada por parâmetro
         * @param {any} id id da polilinha ou objeto contendo o id da polilinha que se deseja calcular a distância
         * @returns {Number} retorna o valor em metros da polilinha encontrada, retorna -1 caso não tenha sido encontrada
         */
        self.getDistance = function (id) {
            var item = findItem("polylines", id.id || id);
            if (!item) {
                if (id.path && id.path.length) {
                    item = {
                        getPath: function () { return id.path; }
                    };
                } else {
                    console.error("O item não foi encontrado ou não é uma polilinha");
                    return -1;
                }
            }
            var res = google.maps.geometry.spherical.computeLength(item.getPath());
            return res;
        };
        var toRad = function (n) { // converte em radianos
            return n * Math.PI / 180;
        };

        var toDeg = function (n) { // converte em graus
            return n * 180 / Math.PI;
        };
        var R = 6378137;// Earth’s mean radius in meter

        var sin = Math.sin, asin = Math.asin, cos = Math.cos, sqrt = Math.sqrt, atan2 = Math.atan2;

        self.getDistanceBetween = function (from, to) {
            if (from instanceof google.maps.Marker) {
                from = from.getPosition();
            }
            if (to instanceof google.maps.Marker) {
                to = to.getPosition();
            }
            var d;
            //if (useGoogleMapsService) {
            //    d = google.maps.geometry.spherical.computeDistanceBetween(from, to);
            //    return d;
            //}
            var dLat = toRad(to.lat() - from.lat());
            var dLon = toRad(to.lng() - from.lng());
            var a = sin(dLat / 2) * sin(dLat / 2) +
                cos(toRad(from.lat())) * cos(toRad(to.lat())) *
                sin(dLon / 2) * sin(dLon / 2);
            var c = 2 * atan2(sqrt(a), sqrt(1 - a));
            d = R * c;
            return d;
        };
        self.existsInPolyline = function (point, polyline, tolerance) {
            if (point instanceof google.maps.Marker) {
                point = point.getPosition();
            }
            if (!(polyline instanceof google.maps.Polyline)) {
                polyline = findItem("polylines", polyline.id || polyline);
            }
            return google.maps.geometry.poly.isLocationOnEdge(point, polyline, tolerance || 0.00001) || //something about 1.5m
                google.maps.geometry.poly.containsLocation(point, polyline);
        };
        var moveTowards = function (from, point, distance) {
            var lat1 = toRad(from.lat());
            var lon1 = toRad(from.lng());
            var lat2 = toRad(point.lat());
            var lon2 = toRad(point.lng());
            var dLon = toRad(point.lng() - from.lng());

            // Descobre a posição a partir do ponto inicial onde será posto o ponto final
            var brng = atan2(sin(dLon) * cos(lat2),
                cos(lat1) * sin(lat2) -
                sin(lat1) * cos(lat2) * cos(dLon));
            var angDist = distance / R;
            // Calculate the destination point, given the source and bearing.
            lat2 = asin(sin(lat1) * cos(angDist) +
                cos(lat1) * sin(angDist) * cos(brng));

            lon2 = lon1 + atan2(sin(brng) * sin(angDist) * cos(lat1),
                cos(angDist) - sin(lat1) * sin(lat2));

            if (isNaN(lat2) || isNaN(lon2)) return null;

            return new google.maps.LatLng(toDeg(lat2), toDeg(lon2));
        };
        /**
         * Calcula a distância necessária a partir de um ponto específico de um path (coleção de posições) e retorna a posição do ponto
         * que está sobre o path e que corresponde a distância especificada
         * @param {path} points instância do path (não pode ser um array) que se quer percorrer
         * @param {Number} distance distância que se quer percorrer sobre o points
         * @param {Number} index indíce do points que se quer iniciar o percurso.
         * @returns {{position: google.maps.LatLng, lastIndex: Number}} retorna um objeto contendo 2 informações:
         * - {google.maps.LatLng} position: posição em que o ponto deve estar
         * - {Number} lastIndex: última posição do points que utilizada no cálculo de distância
         */
        self.getDistanciaFrom = function (points, distance, index) {
            index = index || 0;  // Set index to 0 by default.
            var ini, fim;
            if (index < points.length - 1) { // é o ultimo ponto?
                ini = points.getAt(index);
                fim = points.getAt(index + 1);
                var distanceToNextPoint = google.maps.geometry.spherical.computeLength([ini, fim]);
                //console.log({ index, distanceToNextPoint, polyline });
                if (distance <= distanceToNextPoint) {
                    return { position: moveTowards(ini, fim, distance), lastIndex: index };
                }
                else {
                    return self.getDistanciaFrom(points, distance - distanceToNextPoint, index + 1);
                }
            }
            else {
                return null;
            }
        };

        self.addKmlOnMap = function (url) {
            var kml = new google.maps.KmlLayer({
                url: url,
                map: self.map
            });
            self.kmls.push(kml);
            return kml;
        };
    }
}