import Configuration from "../Configuration";
import * as signalR from "@aspnet/signalr";
export default class WebSocketModel {

    constructor(name) {
        this.name = name;
        this.connect = this.connect.bind(this);
        this.subscribe = this.subscribe.bind(this);
        this.send = this.send.bind(this);
        this._hubName = Configuration.api_url + "/hub/" + this.name;
        this.currentConnection = null;
    }

    connect() {
        var ws = new signalR.HubConnectionBuilder().withUrl(this._hubName)
            .configureLogging(signalR.LogLevel.Information)
            .build();
        this.currentConnection = ws;
        return ws.start();
    }

    subscribe(event, handler) {
        const proxy = this.currentConnection.on(event, handler);
        return proxy;
    }

    send(url) {
        var args = Array.prototype.slice.call(arguments, 1);
        //ws.send('something',);
        const proxy = this.currentConnection.invoke(url, ...args);
        return proxy;
    }

}