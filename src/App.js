import React, { Component } from 'react';
import MainHeader from "./MainHeader";
import MainPage, { Index } from "./MainPage";
import { BrowserRouter as Router } from 'react-router-dom';
import "./bootstrap.css";
import "./App.css";
import configuration from './Configuration';

class App extends Component {
    render() {
        if (configuration.hotSiteOnly) {
            return (
                <Router><div>
                    <MainHeader hotSiteOnly={true} />
                    <Index></Index>
                </div>
                </Router>)
        }
        return (
            <Router>
                <div>
                    <MainHeader />
                    <MainPage />
                </div>
            </Router>
        );
    }
}

export default App;