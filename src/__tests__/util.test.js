import { decryptToken, updateModel, pickProp } from "../Util/Util";

test("test pick prop", () => {
    const obj = {
        a: {
            v: 10,
            c: {
                a: 1
            }
        },
        b: [1, 2, 3, 4, 5],
        c: null,
        d: "testing"
    }

    expect(pickProp(obj, "a.c.a")).toBe(obj.a.c.a);
    expect(pickProp(obj, "b")).toBe(obj.b);
    expect(pickProp(obj, "b.0")).toBe(obj.b[0]);
    expect(pickProp(obj, "d")).toBe(obj.d);
});

const mockState = {
    state: {},
    setState: function (obj) {
        mockState.state = { ...mockState.state, ...obj }
    }
}

test("test bindTo", () => {
    mockState.state = {
        model:{
            a: {
                v: 10,
                c: {
                    a: 1
                }
            },
            b: [1, 2, 3, 4, 5],
            c: null,
            d: "testing"
        }
    }

    mockState.bindTo = updateModel.bind(mockState);

    mockState.bindTo("a.c.a", 15);
    mockState.bindTo("b.2", 100);
    mockState.bindTo("c", "prop C");
    mockState.bindTo("d", { id: 10, name: "teste D" });
    expect(mockState.state.model.a.c.a).toBe(15);
    expect(mockState.state.model.b[2]).toBe(100);
    expect(mockState.state.model.c).toBe("prop C");
    expect(mockState.state.model.d).toEqual({ id: 10, name: "teste D" });
});


it("test not existing property bindTo", () => {
    mockState.state = {
        model:{
            a: {
            }
        }
    }

    mockState.bindTo = updateModel.bind(mockState);

    mockState.bindTo("a.c.a", 15);
    mockState.bindTo("b.2", 100);
    mockState.bindTo("c", "prop C");
    mockState.bindTo("d", { id: 10, name: "teste D" });
    expect(mockState.state.model.a.c.a).toBe(15);
    expect(mockState.state.model.b[2]).toBe(100);
    expect(mockState.state.model.c).toBe("prop C");
    expect(mockState.state.model.d).toEqual({ id: 10, name: "teste D" });
});