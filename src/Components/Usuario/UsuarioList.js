import React, { Component } from "react";
import Usuario from "../../Models/Usuario";
import { Link } from "react-router-dom";
import lodash from "lodash";

export default class UsuarioList extends Component {
    constructor() {
        super();
        this.state = { usuarios: [] };
        this.usuario = new Usuario();
        this.refresh = this.refresh.bind(this);
    }

    componentWillMount() {
        this.refresh();
    }

    refresh() {
        Usuario.get().then((r) => r.json()).then(function (usuarios) {
            console.log(usuarios);
            this.setState({ usuarios });
        }.bind(this));
    }

    render() {
        return (<div>
            <h2>Usuarios</h2>
            <Link to="/usuario/post" className="btn btn-success pull-right">Cadastrar</Link>
            <table className="table">
                <thead>
                    <tr>
                        <th>
                            Email
                        </th>
                        <th>
                            Password
                        </th>
                    </tr>
                </thead>
                <tbody>
                    {
                        lodash.map(this.state.usuarios, function (usuario, i) {
                            return <UsuarioItem refresh={this.refresh} key={usuario.id} model={usuario}></UsuarioItem>
                        }.bind(this))
                    }
                </tbody>
            </table>
        </div>)
    }
}

export class UsuarioItem extends Component {
    constructor() {
        super();
        this.state = { deleteVisible: false };
        this.usuario = new Usuario();
        this.excluirRegistro = this.excluirRegistro.bind(this);
        this.afterDelete = this.afterDelete.bind(this);
    }
    excluirRegistro() {
        this.setState({ deleteVisible: true });
    }

    afterDelete(res) {
        this.setState({ deleteVisible: false });
        if (res) {
            this.usuario.delete(this.props.model.id).then(function () {
                alert("Registro foi excluído com sucesso");
                this.props.refresh && this.props.refresh();
            }).catch(function (r) {
                alert("Registro foi excluído: " + r.message);
            })
        }
    }

    render() {
        return (
            <tr>
                <td>{this.props.model.email} </td>
                <td>{this.props.model.password}</td>
            </tr>);
    }
}