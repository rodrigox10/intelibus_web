import React, { Component } from "react";
import { updateModel, isLoggedIn } from "../../Util/Util";
import Usuario from "../../Models/Usuario";
import { Redirect } from "react-router-dom";
export default class EsqueciSenha extends Component {
    static navigationOptions = {
        title: "Entre ou cadastre-se"
    }

    constructor() {
        super();
        this.state = { model: new Usuario(), modalVisible: true };
        this.updateModel = updateModel.bind(this);
        this.enviar = this.enviar.bind(this);
    }

    enviar() {
        let status = 200;
        this.state.model.post({ url: "esqueciSenha", body: { email: this.state.model.email } }).then((res) => {
            status = res.status;
            if (status >= 200 && status < 400) {
                console.log("Sucesso!", JSON.stringify(res, null, '\t'));
                alert("Foi enviado um e-mail para redefinição de senha para o usuário informado, por favor, entre no seu e-mail e siga os passos para redefinição de sua senha.")
            }
            else {
                console.error("Deu ruim!", res);
                alert("Usuário não existe ou não foi encontrado no banco de dados, certifique-se de que o e-mail digitado corresponde ao e-mail do usuário.")
                // Alert.alert("" + status, JSON.stringify(res, null, '\t'));
            }
        }).catch((er) => {
            console.error("Deu ruim!", er);
            alert("Usuário não existe ou não foi encontrado no banco de dados, certifique-se de que o e-mail digitado corresponde ao e-mail do usuário.")
        });
    }

    render() {
        Redirect
        if(!isLoggedIn()){
            <Redirect to="/login"></Redirect>
        }
        return (
            <div className="row">
                <div className="col-lg-offset-2 col-lg-4">
                    <h3 >Esqueci minha Senha</h3>
                    <div className="content">
                        <form className="form-vertical">
                            <div className="row">
                                <div className="form-group">
                                    <input className="form-control" placeholder="Email Address" onChange={this.updateModel} name="email" value={this.state.model.email} />
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-6">
                                    <button type="button" className="button form-control" onClick={this.enviar}
                                        color="#666" >Redefinir senha </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div >
        );
    }
}