import React, { Component } from "react";
import { updateModel } from "../../Util/Util";
import Usuario from "../../Models/Usuario";
import { Link } from "react-router-dom";
export default class RedefinirSenha extends Component {
    static navigationOptions = {
        title: "Entre ou cadastre-se"
    }

    constructor({ location }) {
        super();
        var query = new URLSearchParams(location.search);
        var params = { token: query.get("token"), uuid: query.get("uuid") };
        if(!params.token && !params.uuid){
            window.location.pathname = "";
            return;
        }
        this.state = { model: new Usuario(), modalVisible: true, token: params.token, uuid: params.uuid };
        this.updateModel = updateModel.bind(this);
        this.enviar = this.enviar.bind(this);
    }

    componentDidMount() {
        let status;
        Usuario.post({
            url:"validarToken",
            body:{
                "uuid": this.state.uuid
            },
            headers:{
                Authorization: "Bearer "+ this.state.token
            }
        }).then(r => {
            status = r.status;
            if (status !== 200) {
                alert("Endereço inválido!");
                window.location.pathname = "";
            } else {
                let model = this.state.model;
                model.uuid = this.state.uuid;
                this.setState({ isLoaded: true, model });
            }
        });
    }

    enviar() {
        let status = 200;
        this.state.model.post({
            url: "redefinirSenha", 
            headers: {
                Authorization: "Bearer " + this.state.token
            }
        }).then((res) => {
            status = res.status;
            if (status >= 200 && status < 400) {
                console.log("Sucesso!", JSON.stringify(res, null, '\t'));
                alert("Sua senha foi alterada com sucesso! Você pode acessar sua conta com a nova senha");
                window.location.pathname = "/login";
            }
            else {
                console.error("Deu ruim!", res);
                alert("Usuário não existe ou não foi encontrado no banco de dados, certifique-se de que o e-mail digitado corresponde ao e-mail do usuário.")
                // Alert.alert("" + status, JSON.stringify(res, null, '\t'));
            }
        }).catch((er) => {
            console.error("Deu ruim!", er);
            alert("Usuário não existe ou não foi encontrado no banco de dados, certifique-se de que o e-mail digitado corresponde ao e-mail do usuário.")
        });
    }

    render() {
        if (!this.state.isLoaded) {
            return <div>Aguarde...</div>
        }
        return (
            <div className="row">
                <div className="col-lg-offset-2 col-lg-4">
                    <h3 >Digitar nova senha</h3>
                    <div className="content">
                        <form className="form-vertical">
                            <div className="row">
                                <div className="form-group">
                                    <input className="form-control" placeholder="Nova Senha" onChange={this.updateModel} name="password" value={this.state.model.password} />
                                </div>
                            </div>
                            <div className="row">
                                <div className="form-group">
                                    <input className="form-control" placeholder="Confirmar Nova Senha" onChange={this.updateModel} name="confirmPassword" value={this.state.model.confirmPassword} />
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-6">
                                    <button type="button" className="button form-control" onClick={this.enviar}
                                        color="#666" >Cadastrar Nova Senha</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div >
        );
    }
}