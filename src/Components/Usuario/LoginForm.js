import React, { Component } from "react";
import { updateModel } from "../../Util/Util";
import Usuario from "../../Models/Usuario";
import { Link } from "react-router-dom";
export default class LoginForm extends Component {
    static navigationOptions = {
        title: "Entre ou cadastre-se"
    }

    constructor() {
        super();
        this.state = { model: new Usuario(), modalVisible: true };
        this.updateModel = updateModel.bind(this);
        this.login = this.login.bind(this);
    }

    login(e) {
        e.preventDefault();
        let status = 200;
        this.state.model.post({ url: "login" }).then((res) => {
            status = res.status;
            return res.json();
        }).then((res) => {
            console.warn("Deu bom!");
            if (status >= 200 && status < 400) {
                console.log("Sucesso!", JSON.stringify(res, null, '\t'));
                localStorage.setItem("token", res.accessToken);
                localStorage.setItem("userData", JSON.stringify(res.userData));
                setTimeout(() => window.location.pathname = "", 1000);
            }
            else {
                console.error("Deu ruim!", res);
                // Alert.alert("" + status, JSON.stringify(res, null, '\t'));
            }
        }).catch((er) => {
            console.error("Deu ruim!", er);
        });
    }

    render() {
        return (
            <div className="row">
                <div className="col-lg-offset-2 col-lg-4">
                    <h3 >Entre ou Cadastre-se</h3>
                    <div className="content">
                        <form className="form-vertical" onSubmit={this.login}>
                            <div className="row">
                                <div className="form-group">
                                    <input className="form-control" placeholder="Email Address" onChange={this.updateModel} name="email" value={this.state.model.email} />
                                </div>
                            </div>
                            <div className="row">
                                <div className="form-group">
                                    <input className="form-control" type="password" placeholder="Password" onChange={this.updateModel} name="password" value={this.state.model.password} />
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-offset-6 col-md-6">
                                    <div className="form-group">
                                        <Link className="control-label" to="/esqueciSenha">Esqueci minha senha</Link>
                                    </div>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-md-6">
                                    <button type="submit" className="button form-control"
                                        color="#666" >Entrar </button>
                                </div>
                                <div className="col-md-6">
                                    <Link to="/usuario/criar">
                                        <button type="button" className="button form-control" color="#666" > Cadastre-se </button>
                                    </Link>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div >
        );
    }
}