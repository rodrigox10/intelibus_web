import React, { Component } from "react";
import { Redirect } from "react-router";
import Cidade from "../../Models/Cidade";
import Usuario from "../../Models/Usuario";
import { updateModel, isLoggedIn } from "../../Util/Util";
import { Select } from "../../Base/Widgets";
export default class LoginForm extends Component {
    constructor() {
        super();
        this.state = { model: new Usuario(), modalVisible: true };
        this.state.model.confirmarSenha = "";
        this.state.model.empresa = {};
        this.state.model.isEmpresa = false;
        this.state.model.passageiro = {};
        this.updateModel = updateModel.bind(this);
        this.cadastrar = this.cadastrar.bind(this);
    }

    cadastrar(e) {
        e.preventDefault();
        let status = 200;
        this.state.model.post({ url: "registrar" }).then((r) => {
            status = r.status;
            r.json();
        }).then((res) => {
            if (status >= 200 && status < 400) {
                window.location.pathname = "/login";
            }
            else {
                alert("" + status, JSON.stringify(res));
            }
        });
    }

    componentDidMount() {
        Cidade.get().then((c) => c.json())
            .then((cidades) => this.setState({ cidades }))
    }

    render() {
        if (isLoggedIn()) {
            return <Redirect to="/"></Redirect>
        }
        return (
            <div className="row">
                <h3>Cadastre-se</h3>
                <form onSubmit={this.cadastrar}>
                    <div className="row">
                        <div className="form-group">
                            <label htmlFor="email" className="control-label">E-mail: </label>
                            <input name="email" onChange={this.updateModel} value={this.state.model.email} className="form-control" />
                            <span className="text-danger"></span>
                        </div>
                    </div>
                    <div className="row">
                        <div className="form-group">
                            <label htmlFor="senha" className="control-label">Senha: </label>
                            <input type="password" name="password" onChange={this.updateModel} value={this.state.model.senha} className="form-control" />
                        </div>
                    </div>
                    <div className="row">
                        <div className="form-group">
                            <label htmlFor="senha" className="control-label">Confirmar Senha: </label>
                            <input type="password" name="confirmarSenha" onChange={this.updateModel} value={this.state.model.confirmarSenha} className="form-control" />
                        </div>
                    </div>
                    <div className="row">
                        <div className="form-group">
                            <label htmlFor="isEmpresa" className="control-label">Empresa? </label>
                            <input type="checkbox" name="isEmpresa" onChange={this.updateModel} checked={this.state.model.isEmpresa} />
                        </div>
                    </div>
                    <div className="row">
                        <div hidden={!this.state.model.isEmpresa} className="form-group">
                            <label htmlFor="empresa.razaoSocial" className="control-label">Razão Social: </label>
                            <input name="empresa.razaoSocial" onChange={this.updateModel} value={this.state.model.empresa.razaoSocial} className="form-control" />
                        </div>
                    </div>
                    <div className="row">
                        <div hidden={!this.state.model.isEmpresa} className="form-group">
                            <label htmlFor="empresa.nomeFantasia" className="control-label">Nome Fantasia: </label>
                            <input name="empresa.nomeFantasia" onChange={this.updateModel} value={this.state.model.empresa.nomeFantasia} className="form-control" />
                        </div>
                    </div>
                    <div className="row">
                        <div hidden={!this.state.model.isEmpresa} className="form-group">
                            <label htmlFor="empresa.identificacao" className="control-label">CPF/CNPJ: </label>
                            <textarea name="empresa.identificacao" onChange={this.updateModel} className="form-control" value={this.state.model.empresa.identificacao}></textarea>
                        </div>
                    </div>
                    <div className="row">
                        <div hidden={!this.state.model.isEmpresa} className="form-group">
                            <label htmlFor="nome" className="control-label">Cidade: </label>
                            <Select id="cidadesOptions" allowCreate={true}
                                items={this.state.cidades} name="empresa.cidade"
                                textfield="nome" callback={this.updateModel} />
                            <span className="text-danger"></span>
                        </div>
                    </div>
                    <div className="row">
                        <div hidden={this.state.model.isEmpresa} className="form-group">
                            <label htmlFor="passageiro.nome" className="control-label">Nome: </label>
                            <input name="passageiro.nome" onChange={this.updateModel} value={this.state.model.passageiro.nome || ""} className="form-control" />
                        </div>
                    </div>
                    <div className="row">
                        <div hidden={this.state.model.isEmpresa} className="form-group">
                            <label htmlFor="passageiro.identificacao" className="control-label">Identidade (RG ou CPF): </label>
                            <textarea name="passageiro.identificacao" onChange={this.updateModel} className="form-control" value={this.state.model.passageiro.identificacao || ""}></textarea>
                        </div>
                    </div>
                    <div className="row">
                        <div className="form-group">
                            <button type="submit" className="btn btn-primary">Cadastrar</button>
                        </div>
                    </div>
                </form>
            </div>
        );
    }
}