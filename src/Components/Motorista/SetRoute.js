import React, { Component } from "react";
import Rota from "../../Models/Rota";
import { Select } from "../../Base/Widgets";
import { updateModel } from "../../Util/Util";
import * as Bootstrap from "react-bootstrap";
import Veiculo from "../../Models/Veiculo";

export class SetRouteModal extends Component {
    constructor() {
        super();
        this.updateModel = updateModel.bind(this);
        this.state = {
            rotas: [],
            selected: null,
            model: {

            }
        };
    }
    componentDidMount() {
        Rota.get().then((r) => r.json()).then((res) => {
            this.setState({ rotas: res });
        });
        Veiculo.get().then((r) => r.json()).then((veiculos) => {
            this.setState({ veiculos });
        });
    }
    componentDidUpdate() {
        document.body.classList[this.props.visible ? "add" : "remove"]("modal-open");
    }
    render() {

        return (<div className="static-modal" hidden={!this.props.visible}>
            <Bootstrap.Modal.Dialog>
                <Bootstrap.Modal.Header>
                    <Bootstrap.Modal.Title>Escolha a rota desejada para o Motorista {this.props.motorista}</Bootstrap.Modal.Title>
                </Bootstrap.Modal.Header>

                <Bootstrap.Modal.Body>
                    <div className="row">
                        <div className="col-lg-12">
                            <div className="form-group">
                                <label>Veiculo: </label>
                                <Select id="rotas" textfield="descricao" items={this.state.veiculos}
                                    name="veiculo"
                                    callback={this.updateModel}>
                                </Select>
                            </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-lg-12">
                            <div className="form-group">
                                <label>Rota {this.state.rotas.length}: </label>
                                <Select id="descricoes" textfield="nome" items={this.state.rotas}
                                    name="rota"
                                    callback={this.updateModel}>
                                </Select>
                            </div>
                        </div>
                    </div>
                </Bootstrap.Modal.Body>

                <Bootstrap.Modal.Footer>
                    <Bootstrap.Button onClick={() => { this.props.callback && this.props.callback(false, this.state.model) }} bsStyle="danger">Cancel</Bootstrap.Button>
                    <Bootstrap.Button onClick={() => { this.props.callback && this.props.callback(true, this.state.model) }} bsStyle="success">OK</Bootstrap.Button>
                </Bootstrap.Modal.Footer>
            </Bootstrap.Modal.Dialog>
        </div>);
    }
}