import React, { Component } from "react";
import Motorista from "../../Models/Motorista";
import Usuario from "../../Models/Usuario";
import { updateModel, simpleExtend } from "../../Util/Util";
import { Select } from "../../Base/Widgets";

export default class MotoristaForm extends Component {
    constructor() {
        super();
        this.state = {
            erros: [],
            model: new Motorista()
        };
        this.updateModel = updateModel.bind(this);
        this.submitForm = this.submitForm.bind(this);
    }

    componentDidMount() {
        const self = this;
        if (this.props.id) {
            Motorista.get(this.props.id).then((r) => r.json()).then(function (motorista) {
                var { model } = self.state;
                simpleExtend(model, motorista);
                self.setState({ model: model });
            });
        }

        Usuario.get().then((r) => r.json()).then(function (usuarios) {
            self.setState({ usuarios });
        });

        this.setState({ texto: this.props.id ? "Editar" : "Criar" });
    }

    submitForm(event) {
        event.preventDefault();
        let status = 200;
        console.log(this.state.model);
        if (this.state.model.id) {
            this.state.model.put().then((r) => { status = r.status; return r.json(); }).then(function (res) {
                if (status == 200)
                    window.location.pathname = "motorista";
            });
        } else {
            this.state.model.post().then((r) => { status = r.status; return r.json(); }).then((res) => {
                if (status == 200)
                    window.location.pathname = "motorista";
                this.setState({ erros: res.erros })
            });
        }
    }

    render() {
        console.log(this.props.id);
        return (
            <div className="row">
                <div className="col-md-4">
                    <h3>{this.props.texto} Motorista</h3>
                    <form onSubmit={this.submitForm}>
                        <div className="form-group">
                            <label htmlFor="nome" className="control-label">Nome: </label>
                            <input required name="nome" onChange={this.updateModel} value={this.state.model.nome} className="form-control" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="nome" className="control-label">Usuário: </label>
                            <Select id="userOptions" allowCreate={false} items={this.state.usuarios} name="usuario" textfield="email" callback={this.updateModel} />
                            <span className="text-danger"></span>
                        </div>
                        <div className="form-group">
                            <button type="submit" className="btn btn-default">{this.state.texto}</button>
                        </div>
                        {this.state.erros.map(function (e, i) {
                            return <p key={i}>{e.key || e}</p>
                        })}
                    </form>
                </div>
            </div>
        );
    }
}