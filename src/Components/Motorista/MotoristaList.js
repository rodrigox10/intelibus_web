import React, { Component } from "react";
import * as Widget from "../../Base/Widgets";
import Motorista from "../../Models/Motorista";
import { Link } from "react-router-dom";
import lodash from "lodash";
import { SetRouteModal } from "./SetRoute";

export default class MotoristaList extends Component {
    constructor() {
        super();
        this.state = { motoristas: [] };
        this.motorista = new Motorista();
        this.refresh = this.refresh.bind(this);
        this.getQRCodes = this.getQRCodes.bind(this);
    }

    componentWillMount() {
        this.refresh();
    }

    refresh() {
        let status = 200;
        this.motorista.get().then((r) => { status = r.status; return r.json(); }).then(function (motoristas) {
            console.log(motoristas);
            if (status == 200)
                this.setState({ motoristas: motoristas });
        }.bind(this)).catch(function (err) {
            alert("Não foi possível listar os motoristas.")
        });
    }

    getQRCodes() {
        let status = 200;
        var self = this;
        Motorista.get("generateQRCodes").then((r) => { status = r.status; return r.json(); }).then(function (qrcodes) {
            if (status === 200) {
                try {
                    if(!qrcodes.identificadores.length){
                        alert("Nenhum motorista em rota foi encontrado!");
                        return;
                    }
                    var WinPrint = window.open('', '', 'left=0,top=0,width=700,height=1000,toolbar=0,scrollbars=0,status=0');

                    if (!WinPrint) {
                        alert("O seu browser está bloqueando o pop-up")
                        return;
                    }
                    var content = "";
                    qrcodes.identificadores.map(function (e) {
                        var motorista = self.state.motoristas.find((m) => m.id === e.motoristaId);
                        var data = new Date(e.criadoEm), { rota } = motorista.itinerarioAtual;
                        //WinPrint.document.write(styles[x].outerHTML || "");
                        content += `<div style="width:100%;text-align:center; margin-left:30px;" class="print-break-page-after">
                        <h2>${motorista.nome}</h2>
                        <h3>${rota.nome} ()</h3>
                        <h4>R$ ${Number(e.tarifaValor).toFixed(2)} - ${data.toLocaleString()}</h4>
                        <img src="https://api.qrserver.com/v1/create-qr-code/?size=500x500&data=${e.id}" />
                        <br>
                        </div>`;
                    });
                    WinPrint.document.write(content);
                    WinPrint.document.close();
                    WinPrint.focus();
                    setTimeout(() => {
                        WinPrint.print();
                    }, 1000);
                } catch (err) {
                    WinPrint.close();
                    console.log(err);
                }
            }
        }).catch(function (err) {
            alert("Não foi possível listar os motoristas.")
        });
    }

    render() {
        return (<div>
            <h2>Motoristas</h2>
            <div style={{ display: "inline-block" }}>
                <Link to="/motorista/criar" className="btn btn-success pull-right">Cadastrar</Link>
                <button onClick={this.getQRCodes} className="btn btn-warning pull-right">Gerar QR Codes</button>
            </div>
            <div>
                <div id="products" className="row list-group">
                    {
                        lodash.map(this.state.motoristas, function (motorista, i) {
                            return <MotoristaItem refresh={this.refresh} key={motorista.id} model={motorista}></MotoristaItem>
                        }.bind(this))
                    }
                </div>
            </div>
        </div>)
    }
}

export class MotoristaItem extends Component {
    constructor() {
        super();
        this.state = { deleteVisible: false };
        this.motorista = new Motorista();
        this.excluirRegistro = this.excluirRegistro.bind(this);
        this.afterDelete = this.afterDelete.bind(this);
        this.setRoute = this.setRoute.bind(this);
    }
    excluirRegistro() {
        this.setState({ deleteVisible: true });
    }

    afterDelete(res) {
        this.setState({ deleteVisible: false });
        if (res) {
            this.motorista.deletar(this.props.model.id).then(r => r.json()).then(() => {
                alert("Registro foi excluído com sucesso");
                this.props.refresh && this.props.refresh();
            }).catch(function (r) {
                alert("Registro foi excluído: " + r.message);
            });
        }
    }

    setRoute(res, route) {
        if (res) {
            Motorista.post({ url: "setRoute/" + this.props.model.id, body: route }).then(r => r.json())
                .then((r) => {
                    alert(`Rota Vinculada Ao Motorista`);
                }).catch( (r) => {
                    this.setState({ erros: r.mensagens });
                });
        }
        this.setState({ setRouteVisible: false });
    }

    render() {
        return (<div key={this.props.model.id} className="item col-xs-4 col-lg-4">
            <div className="thumbnail" style={{ height: "350px" }}>
                <img className="group list-group-image" src={this.props.model.fotoPerfil || "http://placehold.it/250x250/000/fff"} alt="" />
                <div className="caption">
                    <h4 align="center" className="group inner list-group-item-heading">
                        {this.props.model.id} - {this.props.model.nome}
                    </h4>
                    <div className="row">
                        <div className="col-lg-6 col-lg-offset-3">
                            <br />
                            <button onClick={() => this.setState({ setRouteVisible: true })} className="form-control btn btn-danger">Itinerário Atual</button>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-lg-6 col-lg-offset-3">
                            <Link className="form-control btn btn-warning" to={'/motorista/editar/' + this.props.model.id} params={{ id: this.props.model.id }}>Editar</Link>
                        </div>
                    </div>
                </div>
                <SetRouteModal motorista={this.props.model.id} callback={this.setRoute} visible={this.state.setRouteVisible}></SetRouteModal>
                <Widget.DeleteConfirmation callback={this.afterDelete} visible={this.state.deleteVisible}></Widget.DeleteConfirmation>
            </div>
        </div>);
    }
}

