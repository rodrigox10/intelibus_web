import React, { Component } from "react";
import GoogleMapsLoader from "google-maps";
import Configuration from "../../Configuration";

export default class Mapa extends Component {
    constructor() {
        super();
        this.state = {
            instancia: null,
            marcador: null,
            searchAtual: null,
            mapaCarregado: false,
            pontosRota: [],
            caminho: [],
            searchText: "",
            enableMarker: false
        };
        this.loadMap = this.loadMap.bind(this);
        console.log(Configuration);
    }

    loadMap(gmap) {
        const google = window.google;
        const map = new window.google.maps.Map(document.getElementById("mapa_elemento"), {
            center: this.props.center || { lat: -23.397, lng: -50.644 },
            streetViewControl: false,
            mapTypeId: google.maps.MapTypeId[this.props.type],
            zoom: 12
        });
        this.setState({ instancia: map }, function() {
            if(this.props.onLoad){
                this.props.onLoad(map);
            }
        }.bind(this));
    }

    componentDidMount() {
        GoogleMapsLoader.KEY = Configuration.api_key;
        GoogleMapsLoader.VERSION = "weekly";
        GoogleMapsLoader.LIBRARIES = ['geometry', 'places', 'directions'];
        GoogleMapsLoader.load(this.loadMap);
    }

    render() {
        const style = {
            minHeight: "400px"
        };
        return (<div style={style} id="mapa_elemento"></div>);
    }
}
