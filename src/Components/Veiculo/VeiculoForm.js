import React, { Component } from "react";
import Veiculo from "../../Models/Veiculo";
import Usuario from "../../Models/Usuario";
import { updateModel, simpleExtend } from "../../Util/Util";
import { Select } from "../../Base/Widgets";
import Rota from "../../Models/Rota";
export default class VeiculoForm extends Component {
    constructor() {
        super();
        this.state = {
            model: new Veiculo()
        };
        this.updateModel = updateModel.bind(this);
        this.submitForm = this.submitForm.bind(this);
    }

    componentDidMount() {
        const self = this;
        if (this.props.id) {
            Veiculo.get(this.props.id).then((r) => r.json()).then(function (veiculo) {
                var { model } = self.state;
                simpleExtend(model, veiculo);
                console.log("Atualizado!", model);
                self.setState({ model: model });
            });
        }
        this.setState({ texto: this.props.id ? "Editar" : "Criar" });
    }

    submitForm(event) {
        event.preventDefault();
        let status = 200;
        console.log(this.state.model);
        if (this.state.model.id) {
            this.state.model.put().then((r) => { status = r.status; return r.json(); }).then(function (res) {
                window.location.pathname = "veiculo";
            });
        } else {
            this.state.model.post().then((r) => { status = r.status; return r.json(); }).then(function (res) {
                window.location.pathname = "veiculo";
            });
        }
    }

    render() {
        console.log(this.props.id);
        return (
            <div className="row">
                <div className="col-md-4">
                    <h3>{this.props.texto} Veiculo</h3>
                    <form onSubmit={this.submitForm}>
                        <div className="form-group">
                            <label htmlFor="placa" className="control-label">Placa: </label>
                            <input required name="placa" onChange={this.updateModel} value={this.state.model.placa} className="form-control" />
                        </div>
                        <div className="form-group">
                            <label htmlFor="descricao" className="control-label">Descricao: </label>
                            <input required name="descricao" onChange={this.updateModel} value={this.state.model.descricao} className="form-control" />
                            <span className="text-danger"></span>
                        </div>
                        <div className="form-group">
                            <button type="submit" className="btn btn-default">{this.state.texto}</button>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}