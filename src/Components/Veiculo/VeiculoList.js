import React, { Component } from "react";
import * as Widget from "../../Base/Widgets";
import Veiculo from "../../Models/Veiculo";
import { Link } from "react-router-dom";
import lodash from "lodash";

export default class VeiculoList extends Component {
    constructor() {
        super();
        this.state = { veiculos: [] };
        this.veiculo = new Veiculo();
        this.refresh = this.refresh.bind(this);
    }

    componentWillMount() {
        this.refresh();
    }

    refresh() {
        let status = 200;
        this.veiculo.get().then((r) => { status = r.status; return r.json(); }).then(function (veiculos) {
            console.log(veiculos);
            if (status === 200)
                this.setState({ veiculos: veiculos });
        }.bind(this)).catch(function (err) {
            alert("Não foi possível listar os veiculos.")
        });
    }

    render() {
        return (<div>
            <h2>Veiculos</h2>
            <div style={{ display: "inline-block" }}>
                <Link to="/veiculo/criar" className="btn btn-success pull-right">Cadastrar</Link>
            </div>
            <div id="products" className="row list-group">
                {
                    lodash.map(this.state.veiculos, function (veiculo, i) {
                        return <VeiculoItem refresh={this.refresh} key={veiculo.id} model={veiculo}></VeiculoItem>
                    }.bind(this))
                }
            </div>

        </div>)
    }
}

export class VeiculoItem extends Component {
    constructor() {
        super();
        this.state = { deleteVisible: false };
        this.veiculo = new Veiculo();
        this.excluirRegistro = this.excluirRegistro.bind(this);
        this.afterDelete = this.afterDelete.bind(this);
    }
    excluirRegistro() {
        this.setState({ deleteVisible: true });
    }

    afterDelete(res) {
        this.setState({ deleteVisible: false });
        if (res) {
            this.veiculo.deletar(this.props.model.id).then(r => r.json()).then(() => {
                alert("Registro foi excluído com sucesso");
                this.props.refresh && this.props.refresh();
            }).catch(function (r) {
                alert("Registro foi excluído: " + r.message);
            });
        }
    }

    setRoute(res, route) {
        if (res) {
            Veiculo.post({ url: "setRoute", body: { rota: route } }).then(r => r.json())
                .then((r) => {

                }).catch(function (r) {
                    alert("Cadastro falhou: " + r.message);
                });
        }
    }

    render() {
        return (<div key={this.props.model.id} className="item col-xs-4 col-lg-4">
            <div className="thumbnail" style={{ height: "350px" }}>
                <img className="group list-group-image" src={this.props.model.imagem || "http://placehold.it/400x250/000/fff"} alt="" />
                <div className="caption">
                    <h4 align="center" className="group inner list-group-item-heading">
                        {this.props.model.id} - {this.props.model.placa}
                    </h4>
                    <div className="row">
                        <div className="col-lg-12">
                            {this.props.model.descricao}
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-lg-6 col-lg-offset-3">
                            <Link className="form-control btn btn-warning" to={'/veiculo/editar/' + this.props.model.id} params={{ id: this.props.model.id }}>Editar</Link>
                        </div>
                    </div>
                </div>
                <Widget.DeleteConfirmation callback={this.afterDelete} visible={this.state.deleteVisible}></Widget.DeleteConfirmation>
            </div>
        </div>);
    }
}