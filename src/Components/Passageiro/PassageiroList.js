import React, { Component } from "react";
import * as Widget from "../../Base/Widgets";
import Passageiro from "../../Models/Passageiro";
import { Link } from "react-router-dom";
import lodash from "lodash";

export default class PassageiroList extends Component {
    constructor() {
        super();
        this.state = { passageiros: [] };
        this.passageiro = new Passageiro();
        this.refresh = this.refresh.bind(this);
    }

    componentWillMount() {
        this.refresh();
    }

    refresh() {
        this.passageiro.get().then(function (passageiros) {
            console.log(passageiros);
            this.setState({ passageiros: passageiros });
        }.bind(this)).catch(function (err) {
            alert("Não foi possível listar os passageiros.")
        });
    }

    render() {
        return (<div>
            <h2>Passageiros</h2>
            <Link to="/passageiro/criar" className="btn btn-success pull-right">Cadastrar</Link>
            <table className="table">
                <thead>
                    <tr>
                        <th>
                            Nome
                        </th>
                    </tr>
                </thead>
                <tbody>
                    {
                        lodash.map(this.state.passageiros, function (passageiro, i) {
                            return <PassageiroItem refresh={this.refresh} key={passageiro.id} model={passageiro}></PassageiroItem>
                        }.bind(this))
                    }
                </tbody>
            </table>
        </div>)
    }
}

export class PassageiroItem extends Component {
    constructor() {
        super();
        this.state = { deleteVisible: false };
        this.passageiro = new Passageiro();
        this.excluirRegistro = this.excluirRegistro.bind(this);
        this.afterDelete = this.afterDelete.bind(this);
    }
    excluirRegistro() {
        this.setState({ deleteVisible: true });
    }

    afterDelete(res) {
        this.setState({ deleteVisible: false });
        if (res) {
            this.passageiro.delete(this.props.model.id).then(function () {
                alert("Registro foi excluído com sucesso");
                this.props.refresh && this.props.refresh();
            }).catch(function (r) {
                alert("Registro foi excluído: " + r.message);
            })
        }
    }

    render() {
        return (<div key={this.props.model.id} className="item col-xs-4 col-lg-4">
            <div className="thumbnail" style={{ height: "350px" }}>
                <img className="group list-group-image" src={this.props.model.fotoPerfil || "http://placehold.it/400x250/000/fff"} alt="" />
                <div className="caption">
                    <h4 className="group inner list-group-item-heading">
                        {this.props.model.nome}
                    </h4>
                </div>
                <Widget.DeleteConfirmation callback={this.afterDelete} visible={this.state.deleteVisible}></Widget.DeleteConfirmation>
            </div>
        </div>);
    }
}