import React, { Component } from "react";
import Mapa from "../Mapa/Mapa";
import Configuration from "../../Configuration";
import MapService from "../../Base/MapService";
import Rota from "../../Models/Rota";
import Cidade from "../../Models/Cidade";
import * as Widget from "../../Base/Widgets";
import { updateModel } from "../../Util/Util";
export default class RotaIndex extends Component {
    constructor() {
        super();
        this.state = {};
    }

    render() {
        return (<div>
            <h2>Rotas</h2>
            <div>
                <RotaMapa />
            </div>
        </div>)
    }
}

export class RotaMapa extends Component {
    constructor() {
        super();
        this.state = {
            model: new Rota(),
            instancia: null,
            marcador: null,
            searchAtual: null,
            mapaCarregado: false,
            pontosRota: [],
            paradas: [],
            caminho: [],
            searchText: "",
            enableMarker: false,
            cidades: []
        };
        this.onLoadMap = this.onLoadMap.bind(this);
        this.desenhar = this.desenhar.bind(this);
        this.updateModel = updateModel.bind(this);
        this.cadastrarRota = this.cadastrarRota.bind(this);
    }

    componentDidMount() {
        var self = this;
        let status = 200;
        Cidade.get().then((res) => { status = res.status; return res.json(); })
            .then((cidades) => {
                self.setState({ cidades });
            });
    }

    onLoadMap(map) {
        const google = window.google;
        const input = document.getElementById("mapa_searchbox");
        const divsearch = document.getElementById("menu_mapa");
        var searchBox = new window.google.maps.places.SearchBox(input.getElementsByTagName("input")[0]);
        map.controls[google.maps.ControlPosition.LEFT_CENTER].push(divsearch);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function () {
            searchBox.setBounds(map.getBounds());
        });
        var self = this;
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function () {
            var places = searchBox.getPlaces();
            if (places.length == 0) {
                return;
            }
            self.setState({ searchAtual: places[0] });
            // For each place, get the icon, name and location.
            var bounds = new google.maps.LatLngBounds();
            places.forEach(function (place) {
                if (!place.geometry) {
                    console.log("Returned place contains no geometry");
                    return;
                }
                var icon = {
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(25, 25)
                };
                // Create a marker for each place.
                self.state.mapService.addMarkerOnMap({
                    title: place.name,
                    position: place.geometry.location
                });
                if (place.geometry.viewport) {
                    bounds.union(place.geometry.viewport);
                } else {
                    bounds.extend(place.geometry.location);
                }
            });
            map.fitBounds(bounds);
        });
        this.setState({
            instancia: map,
            mapaCarregado: true,
            marcador: new google.maps.Marker({
                position: null,
                map: null
            }),
            mapService: new MapService(map)
        });
    }

    cadastrarRota(event) {
        event.preventDefault();
        let status = 200;
        if (!event.target.noValidate) {
            console.log(this.state.model);
            if (this.state.model.id) {
                this.state.model.put(this.state.model).then((res) => { status = res.status; return res.json(); })
                    .then((rota) => window.location.pathname = "rota");
            } else {
                var { model, caminho, pontosRota } = this.state;
                model.caminho = pontosRota.map((p) => p["formatted_address"]);
                model.paradas = this.state.paradas;
                console.log(model);
                this.state.model.post(this.state.model).then((r) => { status = r.status; return r.json(); })
                    .then((rota) => window.location.pathname = "rota");
            }
        }
    }
    desenhar = () => {
        this.setState({ desenhando: true });
        return this.state.mapService.addStreetPolyline((polyline) => {
            var caminho = polyline.getPath();
            var pontos = [];
            caminho.forEach((p, i) => {
                pontos.push({ indice: i, latitude: p.lat(), longitude: p.lng() });
            });
            var model = this.state.model;
            model.pontos = pontos;
            this.setState({ model, polyline, pontosRota: polyline.caminho });
        });
    }
    render() {
        var self = this;

        return (<div className="mapa">
            <div className="row">
                <div className="col-lg-12">
                    <div className="form-group">
                        <label htmlFor="nome" className="control-label">Nome: </label>
                        <input required name="nome" onChange={this.updateModel} value={this.state.model.nome} className="form-control" />
                        <span className="text-danger"></span>
                    </div>
                </div>
            </div>
            <div className="row">
                <div className="col-lg-4">
                    <div className="form-group">
                        <label htmlFor="nome" className="control-label">Cidade: </label>
                        <Widget.Select className="form-control" id="cidadesOptions" allowCreate={false} items={this.state.cidades} name="regiao" textfield="nome" callback={this.updateModel} />
                        <span className="text-danger"></span>
                    </div>
                </div>
            </div>
            <MenuMapa pontosRota={this.state.pontosRota} hidden={!this.state.mapaCarregado} />
            <div id="mapa_searchbox" hidden={!this.state.mapaCarregado}>
                <input type="search" placeholder="Digite a localização" hidden={this.state.enableMarker} className="input-mapa" />
                <input type="text" disabled hidden={!this.state.enableMarker} className="input-mapa" value={this.state.searchText} />
                <input disabled={this.state.desenhando}
                    type="button"
                    className="btn btn-default"
                    onClick={this.desenhar}
                    defaultValue="Desenhar" />
            </div>
            <Mapa type="ROADMAP" onLoad={this.onLoadMap}></Mapa>
            <button type="button" className="btn btn-success" onClick={this.cadastrarRota}>Cadastrar Rota</button>
        </div>);
    }
}

export class MenuMapa extends Component {
    render() {
        const style = {
            minWidth: "100px",
            maxWidth: "200px",
            minHeight: "300px",
            backgroundColor: "#FFF",
            marginLeft: "10px"
        };
        return (<div id="menu_mapa" style={style}>
            {this.props.pontosRota && this.props.pontosRota.map(function (ponto, i) {
                return (
                    <div key={i} className="menu_mapa_item">
                        <p>{ponto.formatted_address}</p>
                    </div>
                );
            })}
        </div>)
    }
}