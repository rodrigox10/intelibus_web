import React, { Component } from "react";
import Rota from "../../Models/Rota";
import { Link } from "react-router-dom";
import lodash from "lodash";
import { DeleteConfirmation } from "../../Base/Widgets";

export default class RotaList extends Component {
    constructor() {
        super();
        this.state = { rotas: [] };
        this.rota = new Rota();
        this.refresh = this.refresh.bind(this);
    }

    componentWillMount() {
        this.refresh();
    }

    refresh() {
        var self = this;
        Rota.get().then((r) => r.json()).then(function (rotas) {
            console.log(rotas);
            self.setState({ rotas });
        }).catch((ex) => {
            alert(ex.message || ex.errorMessage);
        });
    }
    render() {
        return (<div>
            <h2>Rotas</h2>
            <Link to="/rota/criar" className="btn btn-success pull-right">Cadastrar</Link>
            <table className="table">
                <thead>
                    <tr>
                        <th>
                            ID
                        </th>
                        <th>
                            Nome
                        </th>
                        <th>
                            Região
                        </th>
                        <th>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    {
                        lodash.map(this.state.rotas, function (rota, i) {
                            return <RotaItem key={rota.id} model={rota}></RotaItem>
                        })
                    }
                </tbody>
            </table>
        </div>)
    }
}

export class RotaItem extends Component {
    constructor() {
        super();
        this.state = { deleteVisible: false }
        this.excluirRegistro = this.excluirRegistro.bind(this);
        this.afterDelete = this.afterDelete.bind(this);
    }

    excluirRegistro() {
        this.setState({ deleteVisible: true });
    }

    afterDelete(res) {
        if (res) {
            Rota.deletar(this.props.model.id).then(r => {
                if (r.status === 200) {
                    alert("Registro excluído com sucesso!");
                } else {
                    alert("Falha durante a exclusão do registro!");
                }
                this.setState({ deleteVisible: false });
            }).catch((e) => {
                alert("Falha durante a exclusão do registro!");
                console.log(e);
                this.setState({ deleteVisible: false });
            });
        } else {
            this.setState({ deleteVisible: false });
        }
    }

    render() {
        return (
            <tr>
                <td>{this.props.model.id} </td>
                <td>{this.props.model.nome} </td>
                <td>{this.props.model.regiao.nome} - {this.props.model.regiao.ufDesc}</td>
                <td><a className="btn btn-danger" onClick={this.excluirRegistro}>Excluir</a>
                    <DeleteConfirmation callback={this.afterDelete} visible={this.state.deleteVisible}></DeleteConfirmation>
                </td>
            </tr>);
    }
}