import React, { Component } from "react";
import Mapa from "../Mapa/Mapa";
import MapService from "../../Base/MapService";
import Cidade from "../../Models/Cidade";
import * as Widget from "../../Base/Widgets";
import { updateModel } from "../../Util/Util";
import Motorista from "../../Models/Motorista";
import { Localizacao } from "../../Models/Socket";
import { MenuMapa } from "./RotaCreate";

export default class RotaView extends Component {
    constructor() {
        super();
        this.state = {
            notImplemented: true
        };
    }

    render() {
        return (<div>
            <h3>Linhas</h3>
            <div>
                <RotaViewMapa />
            </div>
        </div>)
    }
}

export class RotaViewMapa extends Component {
    constructor() {
        super();
        this.state = {
            instancia: null,
            searchAtual: null,
            mapaCarregado: false,
            socket: new Localizacao(),
            pontosRota: [],
            searchText: "",
            cidades: [],
            caminho: []
        };
        this.updateModel = updateModel.bind(this);
        this.updateModel = updateModel.bind(this);
        this.onLoadMap = this.onLoadMap.bind(this);
        this.onChangeCidade = this.onChangeCidade.bind(this);
    }

    componentDidMount() {
        var self = this;
        let status = 200;
        this.state.socket.connect().then(() => {
            Cidade.get().then((res) => { status = res.status; return res.json(); })
                .then((cidades) => {
                    self.setState({ cidades });
                })
                .catch((e) => {
                    console.error(e);
                    Cidade.get().then((res) => { status = res.status; return res.json(); })
                        .then((cidades) => {
                            self.setState({ cidades });
                        });
                });
        });
    }

    onChangeCidade(e, region) {
        var self = this;
        this.state.socket.setRegion(region.id);
        var max = (1 << 24) - 1;
        var geraCor = function () {
            let cor = "#";
            let n = Math.floor(Math.random() * max);
            cor += ("00000" + n.toString(16)).substr(-6);
            return cor;
        };
        Motorista.get("getAllMotoristasInRoute?r=" + region.id).then((r) => r.json())
            .then((rotas) => {
                self.setState({ rotas });
                rotas.map((motorista) => {
                    var { rota } = motorista.itinerarioAtual;
                    var color = geraCor();
                    var m = {
                        latitude: 0,
                        longitude: 0,
                        id: motorista.id,
                        fillColor: color,
                        events: {
                            click: function (e) {
                                self.setState({ caminho: rota.caminho });
                            }
                        }
                    };
                    var p = {
                        path: rota.pontos.map((p) => ({ ...p, lat: p.latitude, lng: p.longitude })),
                        id: rota.id,
                        strokeColor: color,
                        strokeWeight: 2
                    };
                    self.state.mapService.addMarker(m);
                    self.state.mapService.addPolyline(p);
                });

                this.state.socket.subscribe("updateGeolocation", function (dados) {
                    dados.id = dados.motorista;
                    self.state.mapService.updateMarker(dados);
                });
                this.state.socket.subscribe("alertarParada", function (dados) {
                    dados.id = dados.motorista;
                    self.state.mapService.addMarker(dados);
                });
            });
    }

    onLoadMap(map) {
        const google = window.google;
        const input = document.getElementById("mapa_searchbox");
        const divsearch = document.getElementById("menu_mapa");
        var searchBox = new window.google.maps.places.SearchBox(input.getElementsByTagName("input")[0]);
        map.controls[google.maps.ControlPosition.BOTTOM_LEFT].push(divsearch);
        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        map.addListener('bounds_changed', function () {
            searchBox.setBounds(map.getBounds());
        });
        var self = this;
        searchBox.addListener('places_changed', function () {
            var places = searchBox.getPlaces();

            if (places.length == 0) {
                return;
            }
            self.setState({ searchAtual: places[0] });
            // Clear out the old markers.
            // For each place, get the icon, name and location.
            var bounds = new google.maps.LatLngBounds();
            places.forEach(function (place) {
                if (!place.geometry) {
                    console.log("Returned place contains no geometry");
                    return;
                }
                var icon = {
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(25, 25)
                };

                if (place.geometry.viewport) {
                    // Only geocodes have viewport.
                    bounds.union(place.geometry.viewport);
                } else {
                    bounds.extend(place.geometry.location);
                }
            });
            map.fitBounds(bounds);
        });
        self.setState({
            instancia: map,
            mapaCarregado: true,
            mapService: new MapService(map)
        });
    }

    render() {
        return (<div className="mapa" >
            <div className="row">
                <div className="col-lg-4">
                    <div className="form-group">
                        <label htmlFor="nome" className="control-label">Selecione a região: </label>
                        <Widget.Select className="form-control" id="cidadesOptions"
                            allowCreate={false}
                            items={this.state.cidades}
                            textfield={(i) => (i.nome + " - " + i.ufDesc)}
                            callback={this.onChangeCidade} />
                        <span className="text-danger"></span>
                    </div>
                </div>
                <div className="col-lg-4">
                    <div className="form-group">
                        <label htmlFor="nome" className="control-label">Selecione a região: </label>
                        <input type="checkbox" id="cidadesOptions"
                            allowCreate={false}
                            items={this.state.cidades}
                            textfield={(i) => (i.nome + " - " + i.ufDesc)}
                            callback={this.onChangeCidade} />
                        <span className="text-danger"></span>
                    </div>
                </div>
            </div>
            <div id="menu_mapa" className="menu_mapa">
                {this.state.caminho.map(function (ponto, i) {
                    return (
                        <div key={i} className="menu_mapa_item">
                            <p>{ponto}</p>
                        </div>
                    );
                })}
            </div>
            <div id="mapa_searchbox" hidden={!this.state.mapaCarregado}>
                <input type="search" hidden={this.state.enableMarker} className="input-mapa" />
                <input type="text" disabled
                    hidden={!this.state.enableMarker}
                    className="input-mapa" value={this.state.searchText} />
            </div>
            <Mapa type="ROADMAP" onLoad={this.onLoadMap}></Mapa>
        </div>);
    }
}