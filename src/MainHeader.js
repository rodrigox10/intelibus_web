import React, { Component } from 'react';
import { Link } from "react-router-dom";
import { isLoggedIn, updateModel, decryptToken } from "./Util/Util";
import Usuario from './Models/Usuario';

class MainHeader extends Component {
    constructor() {
        super();
        this.state = {
            model: {},
            loggedIn: isLoggedIn()
        };
        this.updateModel = updateModel.bind(this);
    }

    componentDidMount() {
        const self = this;
        let status = 200;
        if (!this.props.hotSiteOnly && localStorage.getItem("token")) {
            Usuario.post({ url: "validarToken" }).then((res) => {
                status = res.status;
                if(status !== 200) {
                    localStorage.setItem("token", "");
                    localStorage.setItem("userData", "");
                }
                self.setState({ loggedIn: isLoggedIn() });
            }).catch((er) => {
                console.error("Deu ruim!", er);
            });
        } else {
            localStorage.setItem("token", "");
            localStorage.setItem("userData", "");
        }
    }
    render() {
        var routes = [{
            to: "/rota",
            text: "Linhas e Horários"
        }];
        let userData = {};
        let saldo = 0;
        let isEmpresa = false;
        if (this.props.hotSiteOnly) {
            routes = [];
        }
        else if (isLoggedIn()) {
            var user = decryptToken();

            userData = JSON.parse(localStorage.getItem("userData"));
            saldo = userData.credito ? userData.credito.saldo : 0;
            console.log("Usuario: ", user);
            if (user && user["Grupo"] == 0) {
                isEmpresa = true;
                routes = [
                    {
                        to: "/motorista",
                        text: "Motoristas"
                    }, {
                        to: "/usuario",
                        text: "Usuários"
                    }, {
                        to: "/rota",
                        text: "Rotas"
                    }, {
                        to: "/veiculo",
                        text: "Veículos"
                    }
                ];
            } else {
                routes = [
                    {
                        to: "/rota",
                        text: "Linhas e Horários"
                    }];
            }
        }

        return (
            <nav className="navbar navbar-inverse navbar-fixed-top menu-principal">
                <div className="collapse navbar-collapse">
                    <ul className="navbar-nav nav">
                        <li>
                            <a href="/"><img height="30px" width="150px" className="App-logo" src="./images/logo2.svg" alt="logo" /> </a>
                        </li>
                        {routes.map((r, i) =>
                            (<li key={i} className="nav-item">
                                <Link className="nav-link" to={r.to}>{r.text}</Link>
                            </li>)
                        )}
                    </ul>
                    {this.props.hotSiteOnly ? <span></span> : (<ul className="navbar-nav nav pull-right">
                        <li style={{ display: isLoggedIn() ? "block" : "none" }} className="nav-item">
                            <Link className="nav-link" to="/">Bem-vindo, {userData.nome}</Link>
                        </li>
                        <li style={{ display: isLoggedIn() && !isEmpresa ? "block" : "none" }} className="nav-item">
                            <Link className="nav-link" to="/"><b>R$ {saldo.toFixed(2)}</b></Link>
                        </li>
                        <li style={{ display: isLoggedIn() ? "block" : "none" }} className="nav-item">
                            <Link className="nav-link" to="/logout">Logout</Link>
                        </li>
                        <li style={{ display: !isLoggedIn() ? "block" : "none" }} className="nav-item">
                            <Link className="nav-link" to="/login">Login</Link>
                        </li>
                    </ul>)}
                </div>
            </nav>
        )
    }
}

export default MainHeader;