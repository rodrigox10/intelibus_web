import React from "react";

function updateModel(field, value) {
    const self = this;
    const { model } = self.state;
    if (value) {
        pickProp(model, field, value);
        self.setState({ model });
    } else if (field) {
        switch (field.target.type) {
            case "checkbox":
                pickProp(model, field.target.name, field.target.checked);
                self.setState({ model });
                break;
            case "text":
            case "number":
            default:
                pickProp(model, field.target.name, field.target.value);
                self.setState({ model });
                break;
        }
    }
}

function pickProp(obj, prop, value) {
    let path = prop.split(".");
    let actual = obj;
    for (let index = 0; index < path.length; index++) {
        const c = path[index];
        if (index + 1 == path.length && value !== undefined) {
            actual[c] = value;
        } else if (!actual[c]) {
            actual[c] = {};
        }
        actual = actual[c] || null;
    }
    return actual;
}

function simpleExtend(obj1, obj2, dropIfFalsy){
    if(!obj2) return obj1;
    for (var chave in obj2) {
        obj1[chave] = obj2[chave];
        if(!obj1[chave]){ 
            delete obj1[chave];
        }
    }
    return obj1;
}

export { updateModel, pickProp, simpleExtend };

export class Loading extends React.Component {

}

function decryptToken(tokenString) {
    var token = tokenString || localStorage.getItem("token");
    if (!token || token === "null" || token === "undefined") {
        return localStorage.setItem("token", "");
    }
    var base64Url = token.split('.')[1];
    var base64 = base64Url.replace('-', '+').replace('_', '/');
    return JSON.parse(atob(base64));
}
function isLoggedIn() {
    var token = localStorage.getItem("token");
    if (token === "null" || token === "undefined") {
        localStorage.setItem("token", "");
        return false;
    }
    var res = !!token;
    return res;
}
function sendFetch({ url, method, body, headers }) {
    var token = localStorage.getItem("token");
    return fetch(url, {
        method: method,
        headers: simpleExtend({
            "Authorization": token ? "Bearer " + token : undefined,
            Accept: "application/json",
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Method": "*",
            "Access-Control-Allow-Header": "*"
        }, headers, true),
        body: body ? JSON.stringify(body) : undefined
    });
}
export { decryptToken, isLoggedIn, sendFetch };