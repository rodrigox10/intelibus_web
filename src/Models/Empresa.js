import Crud from "./../Base/Crud";


export default class Empresa extends Crud {
    constructor() {
        super("empresa");
        this.id = 0;
        this.nomeFantasia = "";
    }
}