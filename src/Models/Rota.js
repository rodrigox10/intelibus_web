import Crud from "./../Base/Crud";


export default class Rota extends Crud {
    constructor() {
        super("rota");
        this.id = 0;
        this.nome = "";
        this.temParada = false;
        this.pontos = [];
        this.paradas = [];
        this.caminho = [];
        this.empresa = 0;
    }
}