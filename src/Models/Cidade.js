import Crud from "./../Base/Crud";


export default class Cidade extends Crud {
    constructor() {
        super("cidade");
        this.id = 0;
        this.nome = "";
        this.uf = "";
    }
}