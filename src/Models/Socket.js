import WebSocketModel from "../Base/WebSocket";

export class Localizacao extends WebSocketModel {
    constructor() {
        super("localizacao");
        this.setRegion = this.setRegion.bind(this);
        this.atualizar = this.atualizar.bind(this);
    }

    atualizar(motorista, regionId, latitude, longitude) {
        return this.send("updateGeolocation", motorista, regionId, latitude, longitude)
    }

    setRegion(regionId) {
        if (this.currentConnection) {
            this.send("subscribeToRegion", regionId);
        }
    }
}