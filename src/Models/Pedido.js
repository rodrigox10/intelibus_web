import Crud from "./../Base/Crud";

export default class Pedido extends Crud{
    constructor(){
        super("pedido");
        this.id = 0;
        this.categoria = "";
        this.preco = 0;
        this.nome = "";
    }
}