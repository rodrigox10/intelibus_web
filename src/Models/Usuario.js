import Crud from "./../Base/Crud";

class Usuario extends Crud {
    constructor() {
        super("usuario");
        this.id = 0;
        this.email = "";
        this.password = "";
    }
}

export default Usuario;