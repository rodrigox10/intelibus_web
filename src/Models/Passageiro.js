import Crud from "./../Base/Crud";


export default class Passageiro extends Crud {
    constructor() {
        super("passageiro");
        this.id = 0;
        this.nome = "";
        this.credito = {
            saldo: 0,
            ultimaUtilizacao: null,
            recarregadoEm: null
        };
    }
}