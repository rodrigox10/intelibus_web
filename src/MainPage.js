import React, { Component } from "react";
import { Route, Redirect, Link } from "react-router-dom";

import RotaIndex from "./Components/Rota/RotaCreate";
import UsuarioList from "./Components/Usuario/UsuarioList";
import MotoristaList from "./Components/Motorista/MotoristaList";
import PassageiroList from "./Components/Passageiro/PassageiroList";

import EmpresaForm from "./Components/Empresa/EmpresaForm";
import Cadastro from "./Components/Usuario/Cadastro";
import UsuarioForm from "./Components/Usuario/UsuarioForm";
import MotoristaForm from "./Components/Motorista/MotoristaForm";
import LoginForm from "./Components/Usuario/LoginForm";
import { isLoggedIn, decryptToken } from "./Util/Util";
import RotaView from "./Components/Rota/RotaView";
import RotaList from "./Components/Rota/RotaList";
import VeiculoForm from "./Components/Veiculo/VeiculoForm";
import VeiculoList from "./Components/Veiculo/VeiculoList";
import RedefinirSenha from "./Components/Usuario/RedefinirSenha";
import EsqueciSenha from "./Components/Usuario/EsqueciSenha";

export default class MainPage extends Component {
    render() {
        const logout = <Route exact path="/logout" component={() => { localStorage.setItem("token", ""); localStorage.setItem("userData", ""); return <Redirect to="/"></Redirect> }}></Route>;
        let routes = [
            <Route exact path="/rota" component={RotaView} />,
            <Route exact path="/login" component={LoginForm} />,
            <Route exact path="/redefinirSenha" component={RedefinirSenha} />,
            <Route exact path="/esqueciSenha" component={EsqueciSenha} />,
            <Route exact path="/usuario/criar" component={Cadastro} />];
        if (isLoggedIn()) {
            var user = decryptToken();
            console.log("Usuario: ", user);
            if (user && user["Grupo"] == 0) {
                routes = [
                    <Route exact path="/rota" component={RotaList} />,
                    <Route exact path="/rota/criar" component={RotaIndex} />,
                    <Route exact path="/usuario" component={UsuarioList} />,
                    <Route exact path="/empresa/criar" component={EmpresaForm} />,
                    <Route exact path="/usuario/post" component={UsuarioForm} />,
                    <Route exact path="/motorista" component={MotoristaList} />,
                    <Route exact path="/motorista/criar" component={MotoristaForm} />,
                    <Route exact path="/motorista/editar/:id" render={({ match }) => <MotoristaForm id={match.params.id} />} />,
                    <Route exact path="/veiculo" component={VeiculoList} />,
                    <Route exact path="/veiculo/criar" component={VeiculoForm} />,
                    <Route exact path="/veiculo/editar/:id" render={({ match }) => <VeiculoForm id={match.params.id} />} />,
                    logout
                ];
            } else {
                routes = [
                    <Route exact path="/rota" component={RotaView} />,
                    <Route exact path="/perfil" component={PassageiroList} />,
                    logout
                ];
            }
        }
        return (
            <div>
                <Route exact path="/" component={Index} />
                <div className="container body-container">
                    {routes.map((a, i) => <div key={i}>{a}</div>)}
                </div>
            </div>
        )
    }
}

export class Index extends Component {
    constructor() {
        super();
        this.plusDivs = this.plusDivs.bind(this);
        this.showDivs = this.showDivs.bind(this);
        this.slideIndex = 1;
    }
    plusDivs(n) {
        this.showDivs(this.slideIndex += n);
    }

    showDivs(n) {
        var i;
        var x = document.getElementsByClassName("mySlides");
        if (n > x.length) { this.slideIndex = 1; }
        if (n < 1) { this.slideIndex = x.length; }
        for (i = 0; i < x.length; i++) {
            x[i].style.display = "none";
        }
        x[this.slideIndex - 1].style.display = "block";
    }

    componentDidMount() {
        this.showDivs(this.slideIndex);
    }
    render() {

        return (
            <div>
                <header className="w3-display-container w3-center">
                    <button className="w3-button w3-block w3-green w3-hide-large w3-hide-medium" onClick={() => document.getElementById('download').style.display = 'block'}>Download <i className="fa fa-android"></i> <i className="fa fa-apple"></i></button>
                    <div className="mySlides w3-animate-opacity">
                        <img className="w3-image" src="/images/slider_1.png" alt="Imagem do Slider 1" style={{ minWidth: "500px" }} width="1500" height="1000" />
                        <div className="w3-display-left w3-padding w3-hide-small" style={{ width: "35%" }}>
                            <div className="w3-black w3-opacity w3-hover-opacity-off w3-padding-large w3-round-large">
                                <h1 className="w3-xlarge">Na sua mão e quando você precisar</h1>
                                <hr className="w3-opacity" />
                                <p>Baixe agora mesmo: </p>
                                <p><button className="w3-button w3-block w3-green w3-round" onClick={() => document.getElementById('download').style.display = 'block'}>Download <i className="fa fa-android"></i> <i className="fa fa-apple"></i></button></p>
                            </div>
                        </div>
                    </div>
                    <div className="mySlides w3-animate-opacity">
                        <img className="w3-image" src="/images/slider_2.jpg" alt="Imagem do Slider 2" style={{ minWidth: "500px" }} width="1500" height="1000" />
                        <div className="w3-display-left w3-padding w3-hide-small" style={{ width: "35%" }}>
                            <div className="w3-black w3-opacity w3-hover-opacity-off w3-padding-large w3-round-large">
                                <h1 className="w3-xlarge w3-text-red">Linhas em tempo-real!</h1>
                                <hr className="w3-opacity" />
                                <p>Acompanhe o seu veículo pelo mapa do seu dispositivo</p>
                                <p><Link className="w3-button w3-block w3-red w3-round" to="/rota">Ver Linhas</Link></p>
                            </div>
                        </div>
                    </div>
                    <div className="mySlides w3-animate-opacity">
                        <img className="w3-image" src="/images/slider_3.jpg" alt="Imagem do Slider 3" style={{ minWidth: "500px" }} width="1500" height="1000" />
                        <div className="w3-display-left w3-padding w3-hide-small" style={{ width: "35%" }}>
                            <div className="w3-black w3-opacity w3-hover-opacity-off w3-padding-large w3-round-large">
                                <h1 className="w3-xlarge">Possui empresa?</h1>
                                <hr className="w3-opacity" />
                                <p>Saiba como trabalhar com nosso aplicativo. <a href="">Saiba mais</a></p>
                                <p><button className="w3-button w3-block w3-indigo w3-round" onClick={() => document.getElementById('download').style.display = 'block'}>Download <i className="fa fa-android"></i> <i className="fa fa-apple"></i></button></p>
                            </div>
                        </div>
                    </div>
                    <a className="w3-button w3-black w3-display-right w3-margin-right w3-round w3-hide-small w3-hover-light-grey" onClick={() => this.plusDivs(1)}>Próximo<i className="fa fa-angle-right"></i></a>
                    <a className="w3-button w3-block w3-black w3-hide-large w3-hide-medium" onClick={() => this.plusDivs(1)}>Próximo<i className="fa fa-angle-right"></i></a>
                </header>

                <div className="w3-padding-64 w3-white">
                    <div className="w3-row-padding">
                        <div className="w3-col l8 m6">
                            <h1 className="w3-jumbo"><b>Sobre o aplicativo</b></h1>
                            <h1 className="w3-xxxlarge w3-text-green"><b>Por que usar?</b></h1>
                            <p>Você já reparou como o processo para se pegar um ônibus é longo e cansativo?
                            Você precisa: buscar o local onde quer ir, buscar as rotas que passam por aquele local, olhar os horários e os pontos próximos e sair com antecedência
                            para não perder o ônibus. Aí você embarca e precisa pagar a tarifa com cartão ou dinheiro. Nosso aplicativo possui todas essas funcionalidades (inclusive pagamento)
                            na sua mão e quando você precisar.</p>
                            <button className="w3-button w3-light-grey w3-padding-large w3-section w3-hide-small" onClick={() => document.getElementById('download').style.display = 'block'}>
                                <i className="fa fa-download"></i> Download
                            </button>
                            <p>Available for <i className="fa fa-android w3-xlarge w3-text-green"></i> <i className="fa fa-apple w3-xlarge"></i> <i className="fa fa-windows w3-xlarge w3-text-blue"></i></p>
                        </div>
                        <div className="w3-col l4 m6">
                            <iframe src="https://www.youtube.com/embed/QD5f4EyPRdw" className="w3-image w3-right w3-hide-small" style={{ width: "550px", height: "400px" }}>
                            </iframe>
                            <div className="w3-center w3-hide-large w3-hide-medium">
                                <button className="w3-button w3-block w3-padding-large" onClick={() => document.getElementById('download').style.display = 'block'}>
                                    <i className="fa fa-download"></i> Download Application
                                </button>

                                <iframe src="https://www.youtube.com/embed/QD5f4EyPRdw" className="w3-image w3-right w3-hide-small" width="550" height="450">
                                </iframe>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="download" className="w3-modal w3-animate-opacity">
                    <div className="w3-modal-content" style={{ padding: "32px" }}>
                        <div className="w3-container w3-white">
                            <i onClick={() => document.getElementById('download').style.display = 'none'} className="fa fa-remove w3-xlarge w3-button w3-transparent w3-right w3-xlarge"></i>
                            <h2 className="w3-wide">DOWNLOAD</h2>
                            <p>Download the app in AppStore, Google Play or Microsoft Store.</p>
                            <i className="fa fa-android w3-large"></i> <i className="fa fa-apple w3-large"></i> <i className="fa fa-windows w3-large"></i>
                            <p><input className="w3-input w3-border" type="text" placeholder="Enter e-mail" /></p>
                            <button type="button" className="w3-button w3-block w3-padding-large w3-red w3-margin-bottom" onClick={() => document.getElementById('download').style.display = 'none'}>Fake Download</button>
                        </div>
                    </div>
                </div>

                <div className="w3-padding-64 w3-light-grey">
                    <div className="w3-row-padding">
                        <div className="w3-col l4 m6">
                            <img className="w3-image w3-round-large w3-hide-small w3-grayscale" src="/images/qrcode_1.jpg" alt="App" height="500" />
                        </div>
                        <div className="w3-col l8 m6">
                            <h2 className="w3-jumbo"><b>Pagamento</b></h2>
                            <h2 className="w3-xxxlarge w3-text-red"><b>Entenda como funciona o pagamento</b></h2>
                            <p>O pagamento é direto pelo aplicativo através da leitura de um QR Code, o seu saldo é único e pode ser utilizado
                                para pagar qualquer tarifa de qualquer empresa que utilize nossos serviços, ou seja, você não precisará usar
                                diferentes cartões ou passes quando for para outras cidades, facilidade, simplicidade e agilidade para o seu
                                embarque é com a gente. Comece a utilizar agora mesmo, mas antes, veja se na sua cidade já possui esse serviço:
                                <a href="">Visite a página e veja quem são nossos clientes </a></p>
                        </div>
                    </div>
                </div>

                <div className="w3-padding-64 w3-light-grey">
                    <div className="w3-row-padding">
                        <div className="w3-col l4 m6">
                            <img className="w3-image w3-round-large w3-hide-small w3-grayscale" src="/images/empresa_1.jpg" alt="App" height="500" />
                        </div>
                        <div className="w3-col l8 m6">
                            <h2 className="w3-jumbo"><b>Empresa</b></h2>
                            <h2 className="w3-xxxlarge w3-text-red"><b>Quer saber nossas soluções para empresas?</b></h2>
                            <p>Entenda como utilizar nossos serviços e começar imediatamente na sua região.
                                Forneça aos seus clientes melhor comodidade e facilidade na hora de pegar o ônibus,
                                fique por dentro do comportamento dos usuários, receba relatórios semanais sobre cada uma das linhas,
                                pagamento sem burocracia e tecnologia alinhada ao bom atendimento ao cliente. <a href="">Contate-nos</a></p>
                        </div>
                    </div>
                </div>

                <footer className="w3-container w3-padding-32 w3-light-grey w3-center w3-xlarge">
                    <div className="w3-section">
                        <i className="fa fa-facebook-official w3-hover-opacity"></i>
                        <i className="fa fa-instagram w3-hover-opacity"></i>
                        <i className="fa fa-snapchat w3-hover-opacity"></i>
                        <i className="fa fa-pinterest-p w3-hover-opacity"></i>
                        <i className="fa fa-twitter w3-hover-opacity"></i>
                        <i className="fa fa-linkedin w3-hover-opacity"></i>
                    </div>
                    <p className="w3-medium">Powered by <a href="https://www.w3schools.com/w3css/default.asp" target="_blank" className="w3-hover-text-green">w3.css</a></p>
                </footer>

            </div>);
    }

}